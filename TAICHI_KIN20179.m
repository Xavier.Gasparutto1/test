function TAICHI_KIN20179

%% initialisation et choix du path
%[separator]=init;
Excel = actxserver('Excel.Application');
set(Excel, 'Visible', 1);

close all; clc;
separator='\';
%default_directory=('D:\DataMatlab\Gonio10\Gait_inVivo\Data\Shoulder_Model_Adrien');
%default_directory=('D:\DataMatlab\Gonio10\Gait_inVIVO\Data\Shoulder_Model_Ilyes_FINAL1.1');
default_directory=('D:\DataMatlab\Gonio10\Gait_inVIVO\Data\Shoulder_Model_Claire2');
default_directory=('D:\DataMatlab\Gonio10\TaiChi_kinematics\Data');
%VICON_directory=('J:\Test\FPI2')

OK_VICON_EXTRACT=0;
if OK_VICON_EXTRACT,
    VICON_directory=('F:\Test\Taichi');
    %VICON_directory=('J:\Test\UE_OBPP')
    cd(VICON_directory);
    [~,DimSujetDIR,~,pathinRootVICON,Folder0]=OpenDir('','*',0,'\'); %%open patient directory in vicon directory for 1 to m subjects
    FullPathModelParam=strcat(default_directory,'\MODEL\VICON\Session 1\Param');
    FullPathModelParamPalpa=strcat(default_directory,'\MODEL\VICON\Session 1\PARAMPALPA');
    
    
    for m=1:size(Folder0,1)
        cd(pathinRootVICON(m,:))
        [~,~,~,pathinRootSession,FolderSession]=OpenDir(' ','*',0,'\');  %%open directory for 1 to n sessions
        
        FullPathSESSION=strcat(default_directory,'\',Folder0(m,:),'\',FolderSession);
        FullPathRESU=strcat(default_directory,'\',Folder0(m,:),'\','RESU');
        FullPathFIGURE=strcat(default_directory,'\',Folder0(m,:),'\','FIGURE');
        FullPathPARAM=strcat(default_directory,'\',Folder0(m,:),'\PARAM');
        
        [SUCCESS,MESSAGE,MESSAGEID]=mkdir(FullPathSESSION)
        [SUCCESS,MESSAGE,MESSAGEID]=mkdir(FullPathRESU)
        [SUCCESS,MESSAGE,MESSAGEID]=mkdir(FullPathFIGURE)
        [SUCCESS,MESSAGE,MESSAGEID]=mkdir(FullPathPARAM)
        %mkdir(FullPathDATAOUTPARAM)
        cd(FullPathSESSION)
        %         mkdir ('CALIB'); FullPathSESSIONCALIB=strcat(FullPathSESSION,'\','CALIB');SelectAllCALIB=0;
        mkdir('MOTION');FullPathSESSIONMOTION=strcat(FullPathSESSION,'\','MOTION');SelectAllMOTION=0;
        mkdir('DATAOUT')
        mkdir('PALPA');FullPathSESSIONPALPA=strcat(FullPathSESSION,'\','PALPA');SelectAllPALPA=0;
        mkdir('PARAM');FullPathSESSIONPARAM=strcat(FullPathSESSION,'\','PARAM');SelectAllPARAM=0;
        mkdir('PARAMPALPA');FullPathSESSIONPARAMPALPA=strcat(FullPathSESSION,'\','PARAMPALPA');SelectAllPARAMPALPA=0;
        mkdir('CALIB');FullPathSESSIONCALIB=strcat(FullPathSESSION,'\','CALIB');SelectAllCALIB=0;
        FullPathDATAOUT=strcat(FullPathSESSION,'\','DATAOUT\');
        snapnow;
        cd(FullPathSESSIONMOTION)
        mkdir('STAND');FullPathSESSIONMOTIONSTAND=strcat(FullPathSESSIONMOTION,'\','STAND');SelectAllMOTION=0;
        cd(FullPathDATAOUT)
        mkdir ('analysis')
        mkdir('MOTIONOUT')
        mkdir('PALPAOUT')
        mkdir('FIGURE')
        %         mkdir('PALPAOUT')
        mkdir('CALIBOUT')
        mkdir('REPORT')
        mkdir('RESU')
        mkdir('SAVE')
        FullPathDATAOUTREPORT=strcat(FullPathDATAOUT,'\','REPORT\');
        cd(FullPathDATAOUTREPORT)
        mkdir('FIGURE')
        mkdir('DOC')
        mkdir('TABLE')
        mkdir('VIDEO')
        mkdir('PRESSURE')
        FullPathDATAOUTREPORTDOC=strcat(FullPathDATAOUTREPORT,'\','DOC\');
        
        FullPathDATAOUTREPORTFIGURE=strcat(FullPathDATAOUTREPORT,'\','FIGURE\');
        cd(FullPathDATAOUTREPORTFIGURE)
        %         mkdir('BEST')
        %         mkdir('FAST')
        %         mkdir('BASS')
        mkdir('OUTPDF')
        
        %[S_CALIB_RAW_DATA,DimCALIB,~]=OpenFile('C','*','',pathinRootSession,'C3D',SelectAllCALIB,'r',' ',0);
        %         [S_PALPA_RAW_DATA,DimPALPA,~]=OpenFile('M','*','',pathinRootSession,'C3D',SelectAllPALPA,'r',' ',0);
        [S_MOTION_RAW_DATA,DimMOTION,~]=OpenFile(' ','*','',pathinRootSession,'c3d',SelectAllMOTION,'r');
        [S_PALPA_RAW_DATA,DimPALPA,~]=OpenFile('MKR ','*','',pathinRootSession,'c3d',SelectAllPALPA,'r');
        [S_CALIB_RAW_DATA,DimCALIB,~]=OpenFile('CALIB ','*','',pathinRootSession,'c3d',SelectAllCALIB,'r');
        
        [S_PARAM_RAW_DATA,DimPARAM,~]=OpenFile('P','*','',FullPathModelParam,'m',1,'r');
        [S_PARAMPALPA_RAW_DATA,DimPARAMPALPA,~]=OpenFile('P','*','',FullPathModelParamPalpa,'m ',1,'r');
        [S_PARAMPALPA2_RAW_DATA,DimPARAMPALPA2,~]=OpenFile('Pl','*','',FullPathModelParamPalpa,'C3D ',1,'r');
        [S_PARAMPALPA3_RAW_DATA,DimPARAMPALPA3,~]=OpenFile('Pal','*','',FullPathModelParamPalpa,'srp ',1,'r');
        
        cd(pathinRootSession)
        for ii=1:DimCALIB(2),
            copyfile(S_CALIB_RAW_DATA(ii).name,FullPathSESSIONCALIB);
        end
        for ii=1:DimPALPA(2), copyfile(S_PALPA_RAW_DATA(ii).name,FullPathSESSIONPALPA);    end
        for ii=1:DimMOTION(2), copyfile(S_MOTION_RAW_DATA(ii).name,FullPathSESSIONMOTION);    end
        
        cd(FullPathModelParam)
        for ii=1:DimPARAM(2), copyfile(S_PARAM_RAW_DATA(ii).name,FullPathSESSIONPARAM);    end
        cd(FullPathModelParamPalpa)
        for ii=1:DimPARAMPALPA(2), copyfile(S_PARAMPALPA_RAW_DATA(ii).name,FullPathSESSIONPARAMPALPA);    end
        for ii=1:DimPARAMPALPA2(2), copyfile(S_PARAMPALPA2_RAW_DATA(ii).name,FullPathSESSIONPARAMPALPA);    end
        for ii=1:DimPARAMPALPA3(2), copyfile(S_PARAMPALPA3_RAW_DATA(ii).name,FullPathSESSIONPARAMPALPA);    end
        %for ii=1:DimPARAMPALPA(2),
        
        
    end %%f for m=1:size(Folder0,1)
end  % if OK_VICON_EXTRACT

cd(default_directory);

[~,DimSujet,~,pathinRootTOP,Folder0]=OpenDir('','*',0,'\'); %%open directory for 1 to n subjects


for Sujet=1:size(Folder0,1)
    OK_POPNORM=0;
    MOTIONFOOTKINEMATICS=0;
    POPNORM=0;
    COMPARENORM=0;
    VICON_PROCESS=1;
    
    COMPROCESS_STATIC=1;
    COMPROCESS_CHIKUNG=1;
    CTRL=0;
    CTRL_Chidren=0;
    Adult=0;
    Children=0;
    ALL_FILE=0;
    OK_PROCESS_STATIC=0;
    OK_MOTION_PROCESS=0;
    MOTIONFOOTKINEMATICS=0;
    POPNORM=0;
    COMPARENORM=0;
    FILE.param.CTRL_Chidren=CTRL_Chidren;
    TRANSFERT_REPORT_ALL_dir=strcat(default_directory,'\','TRANSFERT_REPORT\');
    FILE.TRANSFERT_REPORT_ALL_dir=TRANSFERT_REPORT_ALL_dir;
    if Adult,   Resu_ALL_dir=strcat(default_directory,'\','RESU_ALL_Adult\');
    elseif Children,
        Resu_ALL_dir=strcat(default_directory,'\','RESU_ALL_Children\');
    else
        Resu_ALL_dir=strcat(default_directory,'\','RESU_ALL\');
    end
    Resu_ALL_dir_Figure=strcat(Resu_ALL_dir,'Figure\');
    
    Resu_ALL_dir_Norm=strcat(Resu_ALL_dir,'Norm\');
    RightNorm=strcat(Resu_ALL_dir_Norm,'\PopNorm\');
    Resu_ALL_dir_Norm_XLS=strcat(Resu_ALL_dir,'Norm\TABLE\');
    Resu_ALL_dir_Norm_Figure=strcat(Resu_ALL_dir,'Norm\Figure\');
    Resu_ALL_dir_Patient=strcat(Resu_ALL_dir,'Patient\');
    Resu_ALL_dir_Patient_Figure=strcat(Resu_ALL_dir,'Patient\Figure\');
    Resu_ALL_dir_Patient_XLS=strcat(Resu_ALL_dir,'Patient\TABLE\');
    
    Resu_ALL_dir_TABLEALL2=strcat(Resu_ALL_dir,'TABLEALL\');
    Resu_ALL_dir_BENEDETTI=strcat(Resu_ALL_dir,'BENEDETTI\');
    Resu_ALL_dir_COP=strcat(Resu_ALL_dir,'COP\');
    cd(pathinRootTOP(Sujet,:));
    
    
    FILE.SUBJECT.SubjectName=Folder0(Sujet,:);
    FILE.SUBJECT.SubjectCount=Sujet;
    FILE.SUBJECT.Subject_directory=cd;
    FILE.SUBJECT.datout_directory_RESUMAIN=strcat(FILE.SUBJECT.Subject_directory,'\RESU','\');
    FILE.SUBJECT.datout_directory_FIGUREMAIN=strcat(FILE.SUBJECT.Subject_directory,'\FIGURE','\');
    FILE.SUBJECT.datout_directory_FIGUREMAIN_ANGLE=strcat(FILE.SUBJECT.datout_directory_FIGUREMAIN,'ANGLE','\');
    FILE.SUBJECT.Resu_ALL_dir_TABLEALL=Resu_ALL_dir_TABLEALL2;
    FILE.SUBJECT.Resu_ALL_dir_FIGUREALL=Resu_ALL_dir_Figure;
    FILE.SUBJECT.Resu_ALL_dir_COP=Resu_ALL_dir_COP;
    
    FILE.SUBJECT.datout_directory_PARAMMAIN=strcat(FILE.SUBJECT.Subject_directory,'\PARAM','\');
    [~,DimRoot_Session,~,pathinRootSession,FolderSession]=OpenDir('S','*',1,'\');  %%open directory for 1 to n sessions
    
    %     cd(FILE.SUBJECT.datout_directory_PARAMMAIN)
    %     GaitParam;
    %PALPEUR=[EXP1 EXP2 EXP1 EXP2 EXP3 EXP4 EXP3 EXP4 EXP1BTW1 EXP1BTW2];
    %if MOTIONFOOTKINEMATICS
    for Session=1:DimRoot_Session(2)
        FILE.Session=Session;
        FILE.SESSION(Session).SessionNUM=Session;
        FolderSession(Session,:)
        FILE.SESSION(Session).Filename=strcat(FolderSession(Session,:));
        cd(pathinRootSession(Session,:))
        FILE.SESSION(Session).SessionDirectory=cd;
        display(FILE.SESSION(Session).SessionDirectory)
        FILE.SESSION(Session).param_directory=strcat(FILE.SESSION(Session).SessionDirectory,'\','Param');
        FILE.SESSION(Session).parampalpa_directory=strcat(FILE.SESSION(Session).SessionDirectory,'\','Parampalpa');
        cd(FILE.SESSION(Session).param_directory)
        cd(FILE.SESSION(Session).param_directory)
        %cd(FILE.SUBJECT.datout_directory_PARAMMAIN)
        PARAM_GAIT;
        %FILE.SESSION(Session).Observer=char(PALPEUR(1,Session));
        FILE.SESSION(Session).config_directory=strcat(default_directory,'\','CONFIG_CALIB');
        FILE.SESSION(Session).motion_directory=strcat(FILE.SESSION(Session).SessionDirectory,'\','Motion');
        FILE.SESSION(Session).stand_directory=strcat(FILE.SESSION(Session).motion_directory,'\','STAND');
        FILE.SESSION(Session).calib_directory=strcat(FILE.SESSION(Session).SessionDirectory,'\','Calib');
        FILE.SESSION(Session).palpa_directory=strcat(FILE.SESSION(Session).SessionDirectory,'\','Palpa');
        FILE.SESSION(Session).dataout_directory=strcat(FILE.SESSION(Session).SessionDirectory,'\','dataout','\');
        FILE.SESSION(Session).datout_directory_figure=strcat(FILE.SESSION(Session).dataout_directory,'FIGURE','\');
        %FILE.SESSION(Session).datout_directory_SMOOTH=strcat(FILE.SESSION(Session).dataout_directory,'SMOOTH','\');
        FILE.SESSION(Session).datout_directory_SAVE=strcat(FILE.SESSION(Session).dataout_directory,'SAVE','\');
        FILE.SESSION(Session).datout_directory_RESU=strcat(FILE.SESSION(Session).dataout_directory,'RESU','\');
        FILE.SESSION(Session).datout_directory_ANALYSIS=strcat(FILE.SESSION(Session).dataout_directory,'analysis','\');
        FILE.SESSION(Session).datout_directory_MOTIONOUT=strcat(FILE.SESSION(Session).dataout_directory,'MOTIONOUT','\');
        if CTRL_Chidren
            FILE.SESSION(Session).datout_directory_MOTIONOUT=strcat(FILE.SESSION(Session).dataout_directory,'SMOOTH2','\');
            FILE.SESSION(Session).datout_directory_STATICOUT=strcat(FILE.SESSION(Session).datout_directory_MOTIONOUT,'STATIC','\');
            
            FILE.param.Gl2Pelvis=eye(3);
        end
        FILE.SESSION(Session).datout_directory_PALPAOUT=strcat(FILE.SESSION(Session).dataout_directory,'PALPAOUT','\');
        FILE.SESSION(Session).datout_directory_CALIBOUT=strcat(FILE.SESSION(Session).dataout_directory,'CALIBOUT','\');
        FILE.SESSION(Session).datout_directory_MOTIONOUTNORM=strcat(FILE.SESSION(Session).dataout_directory,'MOTIONOUT\NORMDATA','\');
        FILE.SESSION(Session).datout_directory_REPORT=strcat(FILE.SESSION(Session).dataout_directory,'REPORT','\');
        FILE.SESSION(Session).datout_directory_REPORT_TABLE=strcat(FILE.SESSION(Session).dataout_directory,'REPORT\TABLE','\');
        FILE.SESSION(Session).datout_directory_REPORT_DOC=strcat(FILE.SESSION(Session).dataout_directory,'REPORT\DOC','\');
        FILE.SESSION(Session).datout_directory_REPORT_FIGURE=strcat(FILE.SESSION(Session).dataout_directory,'REPORT\FIGURE','\');
        FILE.SESSION(Session).datout_directory_REPORT_VIDEO=strcat(FILE.SESSION(Session).dataout_directory,'REPORT\VIDEO','\');
        FILE.SESSION(Session).datout_directory_REPORT_OUTPDF=strcat(FILE.SESSION(Session).datout_directory_REPORT_FIGURE,'\','OUTPDF','\');
        %FILE.SESSION(Session).datout_directory_REPORT_RETOUR_OUTPDF=strcat(FILE.SESSION(Session).datout_directory_REPORT_FIGURE,'RETOUR','\','OUTPDF','\');
        if VICON_PROCESS
            C3D=struct('fileName',[],'AnalysisMETADATA',[],'ANGLES',[],'FORCES',[],'MOMENTS',[],'POWERS',[],'PARAMETER',[]);%);;
            %N=length(FILE.param.Jointout);
            LC3D=struct('fileName',[],'AnalysisMETADATA',[],'ANGLES',[],'FORCES',[],'MOMENTS',[],'POWERS',[]);%);;
            RC3D=struct('fileName',[],'AnalysisMETADATA',[],'ANGLES',[],'FORCES',[],'MOMENTS',[],'POWERS',[]);
            
            [S,Dim,pathin]=OpenFile('F','* ','',FILE.SESSION(Session).motion_directory,'C3D',0,'r');% conna�t le nom du fichier
            
            for ii=1:Dim(2)
                OUT_ANGLES_BIRDTAIL_X=[];
                OUT_ANGLES_BEGIN_X=[];
                OUT_ANGLES_CLOSE_X=[];
                OUT_ANGLES_BIRDTAIL_Y=[];
                OUT_ANGLES_BEGIN_Y=[];
                OUT_ANGLES_CLOSE_Y=[];
                OUT_ANGLES_BIRDTAIL_Z=[];
                OUT_ANGLES_BEGIN_Z=[];
                OUT_ANGLES_CLOSE_Z=[];
                OUT_POINTS_BIRDTAIL_X=[];
                OUT_POINTS_BIRDTAIL_Y=[];
                OUT_POINTS_BIRDTAIL_Z=[];
                OUT_POINTS_BEGIN_X=[];
                OUT_POINTS_BEGIN_Y=[];
                OUT_POINTS_BEGIN_Z=[];
                OUT_POINTS_CLOSE_X=[];
                OUT_POINTS_CLOSE_Y=[];
                OUT_POINTS_CLOSE_Z=[];
                OUT_ANGLES_header={};
                OUT_POINTS_header={};
                PATHIN=strcat(pathin, separator, S(ii).name);
                acq=btkReadAcquisition(PATHIN);
                [EVENT]=extractEvent(acq);
                C3D(ii).event=extractEvent(acq);
                POINTS=btkGetPoints(acq);
                PointsLabelName=fieldnames(POINTS);
                C3D(ii).POINTS.values=btkGetPoints(acq);
                C3D(ii).POINTS.names=PointsLabelName;
                OK_trunk=0;
                for nn=1:size(PointsLabelName,1)
                    if strcmp(PointsLabelName(nn,:), 'LThoraxAngles')
                        OK_trunk=1;
                    end
                end
                
                for nn=1:size(PointsLabelName,1)
                    if strcmp(PointsLabelName(nn,:), 'CentreOfMass')
                        Index_COM=nn;
                    end
                end
                %OK_trunk=0;
                C3D(ii).OK_trunk=OK_trunk;
                
                for i=1:size(C3D(ii).POINTS.names,1)
                    OUT_POINTS_BEGIN_X=[OUT_POINTS_BEGIN_X C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(1):EVENT.frames(2),1)];
                    OUT_POINTS_BEGIN_Y=[OUT_POINTS_BEGIN_Y C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(1):EVENT.frames(2),2)];
                    OUT_POINTS_BEGIN_Z=[OUT_POINTS_BEGIN_Z C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(1):EVENT.frames(2),3)];
                    OUT_POINTS_BIRDTAIL_X=[OUT_POINTS_BIRDTAIL_X C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(4):EVENT.frames(5),1)];
                    OUT_POINTS_BIRDTAIL_Y=[OUT_POINTS_BIRDTAIL_Y C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(4):EVENT.frames(5),2)];
                    OUT_POINTS_BIRDTAIL_Z=[OUT_POINTS_BIRDTAIL_Z C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(4):EVENT.frames(5),3)];
                    OUT_POINTS_CLOSE_X=[OUT_POINTS_CLOSE_X C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(6):EVENT.frames(7),1)];
                    OUT_POINTS_CLOSE_Y=[OUT_POINTS_CLOSE_Y C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(6):EVENT.frames(7),2)];
                    OUT_POINTS_CLOSE_Z=[OUT_POINTS_CLOSE_Z C3D(ii).POINTS.values.(char(C3D(ii).POINTS.names(i)))(EVENT.frames(6):EVENT.frames(7),3)];
                    OUT_POINTS_header=[ OUT_POINTS_header C3D(ii).POINTS.names(i) ];
                end %% for iii=1:size(C3D(ii).ANGLES.names,1)
                OUT_COP_STATIC_OE=[];
                FILE.OPENEYES='OE';
                
                [hfig20,OUT_COP_FORM_OE,OUT_xls]=extractCOPCOM(ii,acq,FILE,1,30000,OUT_COP_STATIC_OE,'PAGE9','OPEN EYES',EVENT.frames(4),EVENT.frames(5),[OUT_POINTS_BIRDTAIL_X(:,Index_COM) OUT_POINTS_BIRDTAIL_Y(:,Index_COM) OUT_POINTS_BIRDTAIL_Z(:,Index_COM)]);
                %AA=[OUT_POINTS_BIRDTAIL_X(:,Index_COM) OUT_POINTS_BIRDTAIL_Y(:,Index_COM) OUT_POINTS_BIRDTAIL_Z(:,Index_COM)]
                %computationCOM(acq,FILE,[OUT_POINTS_BIRDTAIL_X(:,end-1) OUT_POINTS_BIRDTAIL_Y(:,end-1) OUT_POINTS_BIRDTAIL_Z(:,end-1)],[OUT_POINTS_BIRDTAIL_X(:,end) OUT_POINTS_BIRDTAIL_Y(:,end)]);
                C3D(ii).ANGLES.values=btkGetAngles(acq);
                C3D(ii).ANGLES.names=fieldnames(C3D(ii).ANGLES.values);
                
                for iii=1:size(C3D(ii).ANGLES.names,1)
                    OUT_ANGLES_BEGIN_X=[OUT_ANGLES_BEGIN_X C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(1):EVENT.frames(2),1)];
                    OUT_ANGLES_BIRDTAIL_X=[OUT_ANGLES_BIRDTAIL_X C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(4):EVENT.frames(5),1)];
                    OUT_ANGLES_CLOSE_X=[OUT_ANGLES_CLOSE_X C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(6):EVENT.frames(7),1)];
                    OUT_ANGLES_BEGIN_Y=[OUT_ANGLES_BEGIN_Y C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(1):EVENT.frames(2),2)];
                    OUT_ANGLES_BIRDTAIL_Y=[OUT_ANGLES_BIRDTAIL_Y C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(4):EVENT.frames(5),2)];
                    OUT_ANGLES_CLOSE_Y=[OUT_ANGLES_CLOSE_Y C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(6):EVENT.frames(7),2)];
                    OUT_ANGLES_BEGIN_Z=[OUT_ANGLES_BEGIN_Z C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(1):EVENT.frames(2),3)];
                    OUT_ANGLES_BIRDTAIL_Z=[OUT_ANGLES_BIRDTAIL_Z C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(4):EVENT.frames(5),3)];
                    OUT_ANGLES_CLOSE_Z=[OUT_ANGLES_CLOSE_Z C3D(ii).ANGLES.values.(char(C3D(ii).ANGLES.names(iii)))(EVENT.frames(6):EVENT.frames(7),3)];
                    
                    OUT_ANGLES_header=[OUT_ANGLES_header  C3D(ii).ANGLES.names(iii) ];
                end %% for iii=1:size(C3D(ii).ANGLES.names,1)
                OUT_ANGLES_X_BEGIN_ROM=range(OUT_ANGLES_BEGIN_X);
                OUT_ANGLES_X_BIRDTAIL_ROM=range(OUT_ANGLES_BIRDTAIL_X);
                OUT_ANGLES_X_CLOSE_ROM=range(OUT_ANGLES_CLOSE_X);
                OUT_ANGLES_X_BEGIN_MEDIAN=nanmedian(OUT_ANGLES_BEGIN_X);
                OUT_ANGLES_X_BIRDTAIL_MEDIAN=nanmedian(OUT_ANGLES_BIRDTAIL_X);
                OUT_ANGLES_X_CLOSE_MEDIAN=nanmedian(OUT_ANGLES_CLOSE_X);
                OUT_ANGLES_X_BEGIN_MAX=nanmax(OUT_ANGLES_BEGIN_X);
                OUT_ANGLES_X_BIRDTAIL_MAX=nanmax(OUT_ANGLES_BIRDTAIL_X);
                OUT_ANGLES_X_CLOSE_MAX=nanmax(OUT_ANGLES_CLOSE_X);
                OUT_ANGLES_X_BEGIN_MIN=nanmin(OUT_ANGLES_BEGIN_X);
                OUT_ANGLES_X_BIRDTAIL_MIN=nanmin(OUT_ANGLES_BIRDTAIL_X);
                OUT_ANGLES_X_CLOSE_MIN=nanmin(OUT_ANGLES_CLOSE_X);
                
                OUT_ANGLES_Y_BEGIN_ROM=range(OUT_ANGLES_BEGIN_Y);
                OUT_ANGLES_Y_BIRDTAIL_ROM=range(OUT_ANGLES_BIRDTAIL_Y);
                OUT_ANGLES_Y_CLOSE_ROM=range(OUT_ANGLES_CLOSE_Y);
                OUT_ANGLES_Y_BEGIN_MEDIAN=nanmedian(OUT_ANGLES_BEGIN_Y);
                OUT_ANGLES_Y_BIRDTAIL_MEDIAN=nanmedian(OUT_ANGLES_BIRDTAIL_Y);
                OUT_ANGLES_Y_CLOSE_MEDIAN=nanmedian(OUT_ANGLES_CLOSE_Y);
                OUT_ANGLES_Y_BEGIN_MAX=nanmax(OUT_ANGLES_BEGIN_Y);
                OUT_ANGLES_Y_BIRDTAIL_MAX=nanmax(OUT_ANGLES_BIRDTAIL_Y);
                OUT_ANGLES_Y_CLOSE_MAX=nanmax(OUT_ANGLES_CLOSE_Y);
                OUT_ANGLES_Y_BEGIN_MIN=nanmin(OUT_ANGLES_BEGIN_Y);
                OUT_ANGLES_Y_BIRDTAIL_MIN=nanmin(OUT_ANGLES_BIRDTAIL_Y);
                OUT_ANGLES_Y_CLOSE_MIN=nanmin(OUT_ANGLES_CLOSE_Y);
                
                OUT_ANGLES_Z_BEGIN_ROM=range(OUT_ANGLES_BEGIN_Z);
                OUT_ANGLES_Z_BIRDTAIL_ROM=range(OUT_ANGLES_BIRDTAIL_Z);
                OUT_ANGLES_Z_CLOSE_ROM=range(OUT_ANGLES_CLOSE_Z);
                OUT_ANGLES_Z_BEGIN_MEDIAN=nanmedian(OUT_ANGLES_BEGIN_Z);
                OUT_ANGLES_Z_BIRDTAIL_MEDIAN=nanmedian(OUT_ANGLES_BIRDTAIL_Z);
                OUT_ANGLES_Z_CLOSE_MEDIAN=nanmedian(OUT_ANGLES_CLOSE_Z);
                OUT_ANGLES_Z_BEGIN_MAX=nanmax(OUT_ANGLES_BEGIN_Z);
                OUT_ANGLES_Z_BIRDTAIL_MAX=nanmax(OUT_ANGLES_BIRDTAIL_Z);
                OUT_ANGLES_Z_CLOSE_MAX=nanmax(OUT_ANGLES_CLOSE_Z);
                OUT_ANGLES_Z_BEGIN_MIN=nanmin(OUT_ANGLES_BEGIN_Z);
                OUT_ANGLES_Z_BIRDTAIL_MIN=nanmin(OUT_ANGLES_BIRDTAIL_Z);
                OUT_ANGLES_Z_CLOSE_MIN=nanmin(OUT_ANGLES_CLOSE_Z);                
                
         NameSuject={strcat('COM',32,'FORM',num2str(ii),32,FILE.SUBJECT.SubjectName)};
           
                OUT_ANGLES_X_BIRDTAIL_cell=[ ['SUBJECT' C3D(ii).ANGLES.names' C3D(ii).ANGLES.names'];[ NameSuject num2cell(OUT_ANGLES_X_BEGIN_ROM) num2cell(OUT_ANGLES_X_BIRDTAIL_MAX)]];
                OUT_ANGLES_Y_BIRDTAIL_cell=[ ['SUBJECT' C3D(ii).ANGLES.names' C3D(ii).ANGLES.names'];[ NameSuject num2cell(OUT_ANGLES_Y_BEGIN_ROM) num2cell(OUT_ANGLES_Y_BIRDTAIL_MAX)]];
                OUT_ANGLES_Z_BIRDTAIL_cell=[ ['SUBJECT' C3D(ii).ANGLES.names' C3D(ii).ANGLES.names'];[ NameSuject num2cell(OUT_ANGLES_Z_BEGIN_ROM) num2cell(OUT_ANGLES_Z_BIRDTAIL_MAX)]];
    
                btkDeleteAcquisition(acq);
                
                
                flxlsoutCOP=strcat(FILE.SESSION(FILE.Session).datout_directory_REPORT_TABLE,'COM',32,'FORM',num2str(ii),32,FILE.SUBJECT.SubjectName,'.xlsx');
                flxlsoutCOP_ALL=strcat(FILE.SUBJECT.Resu_ALL_dir_COP,'COP',32,'FORM',num2str(ii),32,FILE.SUBJECT.SubjectName,'.xlsx');
                 xlswrite(flxlsoutCOP,OUT_ANGLES_X_BIRDTAIL_cell,'ANGLE_X');
                  xlswrite(flxlsoutCOP,OUT_ANGLES_Y_BIRDTAIL_cell,'ANGLE_Y');
                 xlswrite(flxlsoutCOP,OUT_ANGLES_Z_BIRDTAIL_cell,'ANGLE_Z');
                 xlswrite(flxlsoutCOP_ALL,OUT_ANGLES_X_BIRDTAIL_cell,'ANGLE_X');
                  xlswrite(flxlsoutCOP_ALL,OUT_ANGLES_Y_BIRDTAIL_cell,'ANGLE_Y');
                 xlswrite(flxlsoutCOP_ALL,OUT_ANGLES_Z_BIRDTAIL_cell,'ANGLE_Z');
               %     xlswrite(flxlsoutCOP_ALL,OUT_xls,'COM');
                
                
                %% [LC3D,OKKineticCYCLE]=normalizeC3Dstruc(ii,FILE,OK_CYCLE,OK_STANCE,OUT_EVENT_CYCLE,OUT_EVENT_STANCE,C3D,LC3D)
            end  %%%for ii=1:Dim(2)
        end  %%%if VICON_PROCESS
        %          %%
        FILE.OK_CLUSTER=0;
        FILE.PREPROCESS=0;
        FILE.PALPALIST='ShoulderScoliose12';
        OK_GRAPH=0;
        OK_FILTER=0;
        % Calibration of the index pulp on finger or hand cluster
        FILE.separator=separator;
        if FILE.PREPROCESS
            REGISTER=[];
            
            [CALIB]=calib_palpatorBTK(FILE);
            CALIB.PALPATOR.RIGHT.MEAN_TIP_TF
            %Registration of Anatomical Landmark on finger cluster
            [REGISTER] =register_palpator_Right2017(FILE,CALIB,REGISTER);
            
            % Registration of Local Anatomical Markers on motion
            RegisterLocalCloudOnTechnicalClusterOnMotion2(FILE,REGISTER);
        end  %%%if FILE.PREPROCESS
        
        if COMPROCESS_STATIC
            PATHIN_STATICOE=strcat(FILE.SESSION(Session).stand_directory, FILE.separator, 'STATIC_OE.c3d');
            acqSTATIC2=btkReadAcquisition(PATHIN_STATICOE);
            POINT_COM_Floor=btkGetPoint(acqSTATIC2,'CentreOfMassFloor');
            POINT_COM_GL=btkGetPoint(acqSTATIC2,'CentreOfMass');
            POINT_COM_GL_range=range(POINT_COM_GL);
            OUT_COP_STATIC_OE=[];
            FILE.OPENEYES='OE';
            [hfig20,fn_PAGE20_pdf,fn_PAGE20_png,OUT_COP_STATIC_OE]=extractCOP(acqSTATIC2,FILE,1,30000,OUT_COP_STATIC_OE,'PAGE9','OPEN EYES');
            btkDeleteAcquisition(acqSTATIC2);
            %                         end
            OK_CLOSEEYES=1;
            if OK_CLOSEEYES
                PATHIN_STATICCE=strcat(FILE.SESSION(Session).stand_directory, FILE.separator,  'STATIC_CE.c3d');
                acqSTATIC3=btkReadAcquisition(PATHIN_STATICCE);
                FILE.OPENEYES='CE';
                OUT_COP_STATIC_CE=[];
                [hfig30,fn_PAGE30_pdf,fn_PAGE30_png,OUT_COP_STATIC_CE]=extractCOP(acqSTATIC3,FILE,1,30000,OUT_COP_STATIC_CE,'PAGE10','CLOSED EYES');
                btkDeleteAcquisition(acqSTATIC3);
                
            end  %%if OK_CLOSEEYES
        end  %%if COMPROCESS_STATIC
        if COMPROCESS_CHIKUNG
            
            [S_CHI,DimCHI,pathin]=OpenFile('CHI','* ','',FILE.SESSION(Session).stand_directory,'C3D',1,'r');% conna�t le nom du fichier
            if DimCHI(2)>1
                acqSTATIC1=btkReadAcquisition(strcat(pathin,'\',S_CHI(1).name));
                acqSTATIC2=btkReadAcquisition(strcat(pathin,'\',S_CHI(2).name));
                acqSTATIC3=btkMergeAcquisitions(acqSTATIC1,acqSTATIC2);
            else
                acqSTATIC3=btkReadAcquisition(pathin);
            end
            OUT_COP_STATIC_OE=[];
            FILE.OPENEYES='OE';
            [hfig40,fn_PAGE40_pdf,fn_PAGE40_png,OUT_COP_CHIkung_OE]=extractCOP(acqSTATIC3,FILE,1,30000,OUT_COP_STATIC_OE,'PAGE11','CHIKUNG');
            btkDeleteAcquisition(acqSTATIC3);
            %                         end
        end  %%% if COMPROCESS_CHIKUNG
        
        if MOTIONFOOTKINEMATICS
            if OK_PROCESS_STATIC
                
                switch ALL_FILE
                    case 1
                        [S_Static,Dim_STATIC,pathin]=OpenFile('*_','STA* ','',FILE.SESSION(Session).datout_directory_STATICOUT,'C3D',1,'r');% conna�t le nom du fichier
                    case 0
                        [S_Static,Dim_STATIC,pathin]=OpenFile('*_','STA*','',FILE.SESSION(Session).datout_directory_STATICOUT,'C3D',1,'r');% conna�t le nom du fichier
                end %if ALL_FILE
                cd(default_directory);
                STATIC=1;
                for j=1:Dim_STATIC(2)
                    
                    FileName_REF=S_Static(j).name;
                    
                    
                    
                    
                    K=strfind(FileName_REF,'_');
                    FILE.SESSION(Session).trialname=strcat(FILE.SESSION(Session).Filename,32,FileName_REF);
                    FILE.SESSION(Session).trialcount=j;
                    
                    
                    %SIDE_OF_TEST_REF=FileName_REF(1:K(1)-1);
                    FILE.param.JOINT_REF=FileName_REF(K(1)+1:K(2)-1);
                    
                    FILE.param.TASK=FileName_REF(K(2)+1:K(3)-1);
                    FILE.param.MODALITY=FileName_REF(K(3)+1:K(4)-1);
                    
                    FILE.param.BILATERALITY=FileName_REF(K(4)+1:K(5)-1);
                    
                    PATHIN_STATIC=strcat(pathin, separator, S_Static(j).name);
                    acqSTATIC=btkReadAcquisition(PATHIN_STATIC);
                    
                    %                     FILE.param.ClusterNameKinematicModel_AL={'PELVIS_AL';'THORAX';'THORAX_AL';'RCLAVICLE_AL';'RCLAVICLE2_AL';'RSCAPULA';'RSCAPULA_AL';'RHUMERUS';'RHUMERUS_AL';'RHUMERUS3_AL';...
                    %                         'RFOREARM';'RFOREARM_AL';'RHAND';'RWRIST_AL';...
                    %                          'LCLAVICLE_AL'; 'LCLAVICLE2_AL';'LSCAPULA';'LSCAPULA_AL';'LHUMERUS';'LHUMERUS_AL';'LHUMERUS3_AL';...
                    %                          'LFOREARM';'LFOREARM_AL';'LHAND';'LWRIST_AL'};
                    %                      FILE.param.JointModel={'Global Thorax_AL';'Pelvis_AL Thorax_AL';'Thorax_AL RClavicle_AL';'Thorax_AL LClavicle_AL';'Thorax_AL RClavicle2_AL';'Thorax_AL LClavicle2_AL';...
                    %                          'RClavicle_AL RScapula_AL';'LClavicle_AL LScapula_AL';...
                    %                          'Thorax_AL RScapula_AL';'Thorax_AL LScapula_AL';'Thorax_AL RHumerus_AL';'Thorax_AL LHumerus_AL';'Thorax_AL RHumerus3_AL';'Thorax_AL LHumerus3_AL';'RHumerus_AL RForearm_AL';'LHumerus_AL LForearm_AL';...
                    %                          'RForearm_AL RWrist_AL';'LForearm_AL LWrist_AL'};
                    
                    %                     FILE.param.ClusterNameKinematicModel_AL={'PELVIS_AL';'THORAX';'THORAX_AL';'CLAVICLE_AL';'CLAVICLE2_AL';'SCAPULA';'SCAPULA_AL';'HUMERUS';'HUMERUS_AL';'HUMERUS3_AL';...
                    %                         'FOREARM';'FOREARM_AL';'HAND';'WRIST_AL'};
                    %                          'LCLAVICLE_AL'; 'LCLAVICLE2_AL';'LSCAPULA';'LSCAPULA_AL';'LHUMERUS';'LHUMERUS_AL';'LHUMERUS3_AL';...
                    %                          'LFOREARM';'LFOREARM_AL';'LHAND';'LWRIST_AL'
                    FILE.param.ClusterNameKinematicModel_AL={'THORAX';'PELVIS_AL';'GLOBAL';'THORAX_AL';'CLAVICLE_AL';'CLAVICLE2_AL';'SCAPULA';'SCAPULA_AL';'HUMERUS';'HUMERUS_AL';'HUMERUS3_AL';...
                        'FOREARMY_AL';'FOREARMZ_AL';'HANDY_AL';'HANDZ_AL'};%%%'WRISTY0_AL';;'HANDif OK_FULL_PELVIS_AL,_AL''WRISTY_AL';'WRISTZ_AL'
                    
                    %                     FILE.param.JointModel={'Pelvis_AL Thorax_AL';'Thorax_AL Clavicle2_AL';'Clavicle2_AL Scapula_AL';'Thorax_AL Scapula_AL';'Thorax_AL Humerus_AL';'Scapula_AL Humerus_AL';...
                    %                         'Thorax_AL Humerus3_AL';'Humerus_AL Forearm_AL';'Forearm_AL Wrist_AL'};
                    
                    FILE.param.JointModel={'Pelvis_AL Thorax_AL';'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Clavicle2_AL';...
                        'Thorax_AL Scapula_AL';'Thorax_AL Humerus_AL';'Thorax_AL Humerus3_AL';...
                        'Clavicle_AL Scapula_AL';'Scapula_AL Humerus_AL';'Scapula_AL Humerus3_AL';...
                        'Humerus3_AL ForearmY_AL';'Humerus3_AL ForearmZ_AL';...
                        'Humerus_AL ForearmY_AL';'Humerus_AL ForearmZ_AL';...
                        'ForearmY_AL HandY_AL';'ForearmZ_AL HandZ_AL'};%%'WristY_AL WristZ_AL'
                    
                    
                    if (strcmp(FILE.param.BILATERALITY,'ONEL'))|| (strcmp(FILE.param.BILATERALITY,'BOTH'))
                        FILE.param.SIDE=-1;
                        [FILE]=attributeClusterUA(acqSTATIC,FILE,STATIC,'STATIC');
                        [LSEGMENT_AL_REF,FILE]=extractSEGMENTstruct_REF(acqSTATIC,FILE,STATIC,'STATIC',FILE.param.Gl2Pelvis);
                        SIDE_OF_TEST_REF='L';
                        LJOINTREF(length(FILE.param.JointModel))=struct('name',[],'NFrame',[],'EULERZXY',[],'OVP',[],'GES',[]);%);
                        [LJOINTREF,FILE]=extractJOINTstruct_REF(acqSTATIC,FILE,LJOINTREF,LSEGMENT_AL_REF,STATIC,SIDE_OF_TEST_REF,'STATIC');
                    end
                    if (strcmp(FILE.param.BILATERALITY,'ONER'))|| (strcmp(FILE.param.BILATERALITY,'BOTH'))
                        FILE.param.SIDE=1;
                        [FILE]=attributeClusterUA(acqSTATIC,FILE,STATIC,'STATIC');
                        [RSEGMENT_AL_REF,FILE]=extractSEGMENTstruct_REF(acqSTATIC,FILE,STATIC,'STATIC',FILE.param.Gl2Pelvis);
                        SIDE_OF_TEST_REF='R';
                        %RJOINTREF=struct( );
                        RJOINTREF(length(FILE.param.JointModel ))=struct('name',[],'NFrame',[],'EULERZXY',[],'OVP',[],'GES',[]);%);
                        [RJOINTREF,FILE]=extractJOINTstruct_REF(acqSTATIC,FILE,RJOINTREF,RSEGMENT_AL_REF,STATIC,SIDE_OF_TEST_REF,'STATIC');
                        
                    end
                    
                    
                end
            end %if OK_PROCESS_STATIC
            
            if OK_MOTION_PROCESS
                switch ALL_FILE
                    case 1
                        [S_Motion,Dim_MOTION,pathin]=OpenFile('LR_','* ','',FILE.SESSION(Session).datout_directory_MOTIONOUT,'C3D',1,'r');% conna�t le nom du fichier
                    case 0
                        [S_Motion,Dim_MOTION,pathin]=OpenFile('LR_','* ',' ',FILE.SESSION(Session).datout_directory_MOTIONOUT,'C3D',0,'r');% conna�t le nom du fichier
                end %if ALL_FILE
                cd(default_directory);
                STATIC=0;
                
                
                %                hfig2=figure(10);hfig3=figure(11);
                for j=1:Dim_MOTION(2)
                    
                    FileName=S_Motion(j).name
                    K=strfind(FileName,'_');
                    %                     FILE.SESSION(Session).trialname=strcat(FILE.SESSION(Session).Filename,32,FileName);
                    %                     FILE.SESSION(Session).trialcount=j;
                    
                    %SIDE_OF_TEST_REF=FileName_REF(1:K(1)-1);
                    FILE.param.JOINT=FileName(K(1)+1:K(2)-1);
                    
                    FILE.param.TASK=FileName(K(2)+1:K(3)-1);
                    FILE.param.MODALITY=FileName(K(3)+1:K(4)-1);
                    
                    FILE.param.BILATERALITY=FileName(K(4)+1:K(5)-1);
                    
                    PATHIN_MOTION=strcat(pathin, separator, S_Motion(j).name);
                    acq=btkReadAcquisition(PATHIN_MOTION);
                    
                    switch FILE.param.TASK
                        case 'ABD'
                            NFIG=10;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='ab_adduction';
                        case 'BRI'
                            NFIG=20;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='mettre la main derri�re le dos (RI)';
                        case 'COOKIES'
                            NFIG=30;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='mettre la main � la bouche';
                        case 'EP'
                            NFIG=40;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='metrre la main � l''�paule oppos�e''';
                        case 'FL'
                            NFIG=50;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='flexion-extension';
                        case 'MT'
                            NFIG=60;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='mettre la main derri�re la t�te';
                        case 'PROSUP'
                            NFIG=70;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='prosupination de l''avant-bras';
                        case 'RIRE'
                            NFIG=80;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='rotation axiale de l''�paule en R1';
                        case 'R2'
                            NFIG=90;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='rotation axiale de l''�paule en R2';
                        case 'SRI'
                            NFIG=100;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='mettre la main � la poche';
                        case 'TRANSUP'
                            NFIG=110;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='�l�vation des �paules';
                        case 'ME'
                            NFIG=120;
                            [Pathout_FIG]=createdir2(FILE.SESSION(Session).datout_directory_figure,FILE.param.TASK);
                            [Pathout_SAVE]=createdir2(FILE.SESSION(Session).datout_directory_SAVE,FILE.param.TASK);
                            [Pathout_RESU]=createdir2(FILE.SESSION(Session).datout_directory_RESU,FILE.param.TASK);
                            LabelName='mettre la main � l''�paule oppos�e';
                    end
                    
                    if (strcmp(FILE.param.BILATERALITY,'ONEL'))|| (strcmp(FILE.param.BILATERALITY,'BOTH'))
                        
                        FILE_LMVT.SUBJECT.SubjectName=FILE.SUBJECT.SubjectName;
                        FILE_LMVT.SESSION.Filename=FILE.SESSION.Filename;
                        FILE_LMVT.SESSION(Session).trialname=FILE.SESSION(Session).trialname;
                        FILE_LMVT.param.ClusterNameKinematicModel_AL=FILE.param.ClusterNameKinematicModel_AL;
                        FILE_LMVT.param.JointModel=FILE.param.JointModel;
                        FILE_LMVT.SESSION.datout_directory_ANALYSIS=FILE.SESSION.datout_directory_ANALYSIS;
                        FILE_LMVT.param.BILATERALITY=FILE.param.BILATERALITY;
                        FILE_LMVT.param.JOINT=FILE.param.JOINT;
                        FILE_LMVT.param.TASK=FILE.param.TASK;
                        FILE_LMVT.param.MODALITY=FILE.param.MODALITY;
                        FILE_LMVT.param.SIDE=-1;
                        FILE_LMVT=attibuteCutValues(acq,FILE_LMVT);
                        FILE_LMVT.param.CTRL_Chidren =FILE.param.CTRL_Chidren;
                        
                        [FILE_LMVT]=attributeClusterUA(acq,FILE_LMVT,STATIC,'MVT');
                        [LSEGMENT_AL,FILE_LMVT]=extractSEGMENTstruct_REF(acq,FILE_LMVT,STATIC,'MVT',FILE.param.Gl2Pelvis);
                        SIDE_OF_TEST='L';
                        LJOINT(length(FILE_LMVT.param.JointModel ))=struct('name',[],'NFrame',[],'EULERZXY',[],'OVP',[],'GES',[]);%);
                        [LJOINT,FILE_LMVT]=extractJOINTstruct_MVT(acq,FILE_LMVT,LJOINT,LJOINTREF,LSEGMENT_AL,LSEGMENT_AL_REF,STATIC,SIDE_OF_TEST,'MVT');
                        [LJOINT,FILE_LMVT]=normalizeJOINTstrucSHOULDER(FILE_LMVT,LJOINT);
                        
                        %                     FILE.param.JointModel={'Pelvis_AL Thorax_AL';'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Clavicle2_AL';...
                        %                         'Thorax_AL Scapula_AL';'Thorax_AL Humerus_AL';'Thorax_AL Humerus3_AL';...
                        %                         'Clavicle_AL Scapula_AL';'Scapula_AL Humerus_AL';'Scapula_AL Humerus3_AL';...
                        %                         'Humerus3_AL ForearmY_AL';'Humerus3_AL ForearmZ_AL';...
                        %                         'Humerus_AL ForearmY_AL';'Humerus_AL ForearmZ_AL';...
                        %                         'ForearmZ_AL WristZ_AL';'ForearmY_AL WristY_AL';'WristY_AL WristZ_AL'};
                        %
                        
                        
                        %                         {'Pelvis_AL Thorax_AL'; 1
                        %                             'Global Thorax_AL';2
                        %                             'Thorax_AL Clavicle_AL';3
                        %                             'Thorax_AL Clavicle2_AL';4
                        %                             'Thorax_AL Scapula_AL';5
                        %                             'Thorax_AL Humerus_AL';6
                        %                             'Thorax_AL Humerus3_AL';7
                        %                             'Clavicle_AL Scapula_AL';8
                        %                             'Scapula_AL Humerus_AL';9
                        %                             'Scapula_AL Humerus3_AL';10
                        %                             'Humerus3_AL ForearmY_AL';11
                        %                             'Humerus3_AL ForearmZ_AL';12
                        %                             'Humerus_AL ForearmY_AL';13
                        %                             'Humerus_AL ForearmZ_AL';14
                        %                             'ForearmZ_AL WristZ_AL';15
                        %                             'ForearmY_AL WristY_AL';16
                        %                             'WristY_AL WristZ_AL'}17
                        
                        %%% {'Pelvis_AL Thorax_AL';'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Clavicle2_AL';'Thorax_AL Scapula_AL';...
                        %%%'Thorax_AL Humerus_AL';'Thorax_AL Humerus3_AL';'Clavicle_AL Scapula_AL';'Scapula_AL Humerus_AL';'Scapula_AL Humerus3_AL';
                        %%%'Humerus3_AL ForearmY_AL';'Humerus3_AL ForearmZ_AL';'Humerus_AL ForearmY_AL';'Humerus_AL ForearmZ_AL';'ForearmY_AL HandY_AL';'ForearmZ_AL HandZ_AL'}
                        
                        FILE_LMVT.param.JointGraphSelectionBONE={'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Scapula_AL';'Thorax_AL Humerus_AL';...
                            'Humerus3_AL ForearmY_AL';'ForearmY_AL HandY_AL'};
                        %                         FILE.param.JointGraphSelectionLOCAL={'Pelvis_AL Thorax_AL';'Thorax_AL Clavicle2_AL';'Clavicle2_AL Scapula_AL';'Scapula_AL Humerus_AL';..
                        %                             'Humerus_AL Forearm_AL';'Forearm_AL Wrist_AL'};
                        %                         LJOINT(6).EULERNORM.ALLER.angle0(:,1,:)= -LJOINT(6).EULERNORM.ALLER.angle0(:,1,:);
                        %                         LJOINT(6).EULERNORM.ALLER.angle0(:,2,:)= -LJOINT(6).EULERNORM.ALLER.angle0(:,2,:);
                        %                         LJOINT(7).EULERNORM.ALLER.angle0(:,1,:)= -LJOINT(7).EULERNORM.ALLER.angle0(:,1,:);
                        %                         LJOINT(7).EULERNORM.ALLER.angle0(:,2,:)= -LJOINT(7).EULERNORM.ALLER.angle0(:,2,:);
                        
                        LJOINT(11).EULERNORM.ALLER.angle0(:,3,:)=LJOINT(12).EULERNORM.ALLER.angle0(:,3,:);%%'Humerus3_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus3_AL ForearmZ_AL'
                        LJOINT(13).EULERNORM.ALLER.angle0(:,3,:)=LJOINT(14).EULERNORM.ALLER.angle0(:,3,:);%%'Humerus_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus_AL ForearmZ_AL'
                        LJOINT(15).EULERNORM.ALLER.angle0(:,2,:)=LJOINT(16).EULERNORM.ALLER.angle0(:,1,:);
                        %
                        %                         LJOINT(6).EULERNORM.RETOUR.angle0(:,1,:)= -LJOINT(6).EULERNORM.RETOUR.angle0(:,1,:);
                        %                         LJOINT(6).EULERNORM.RETOUR.angle0(:,2,:)= -LJOINT(6).EULERNORM.RETOUR.angle0(:,2,:);
                        %                         LJOINT(7).EULERNORM.RETOUR.angle0(:,1,:)= -LJOINT(7).EULERNORM.RETOUR.angle0(:,1,:);
                        %                         LJOINT(7).EULERNORM.RETOUR.angle0(:,2,:)= -LJOINT(7).EULERNORM.RETOUR.angle0(:,2,:);
                        LJOINT(11).EULERNORM.RETOUR.angle0(:,3,:)=LJOINT(12).EULERNORM.RETOUR.angle0(:,3,:);
                        LJOINT(13).EULERNORM.RETOUR.angle0(:,3,:)=LJOINT(14).EULERNORM.RETOUR.angle0(:,3,:);
                        LJOINT(15).EULERNORM.RETOUR.angle0(:,2,:)=LJOINT(16).EULERNORM.RETOUR.angle0(:,1,:);
                        
                        %                         LJOINT(6).EULERZXY.angleM0(:,1,:)= -LJOINT(6).EULERZXY.angleM0(:,1,:);
                        %                         LJOINT(6).EULERZXY.angleM0(:,2,:)= -LJOINT(6).EULERZXY.angleM0(:,2,:);
                        %
                        %                         LJOINT(7).EULERZXY.angleM0(:,1,:)= -LJOINT(7).EULERZXY.angleM0(:,1,:);
                        %                         LJOINT(7).EULERZXY.angleM0(:,2,:)= -LJOINT(7).EULERZXY.angleM0(:,2,:);
                        
                        LJOINT(11).EULERZXY.angleM0(:,3,:)=LJOINT(12).EULERZXY.angleM0(:,3,:);%%'Humerus3_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus3_AL ForearmZ_AL'
                        LJOINT(13).EULERZXY.angleM0(:,3,:)=LJOINT(14).EULERZXY.angleM0(:,3,:);%%'Humerus_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus_AL ForearmZ_AL'
                        LJOINT(15).EULERZXY.angleM0(:,2,:)=LJOINT(16).EULERZXY.angleM0(:,1,:);
                        
                        FILE_LMVT.param.JointGraphSelectionBONETable={'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Scapula_AL';'Thorax_AL Humerus3_AL';...
                            'Humerus3_AL ForearmY_AL';'ForearmY_AL HandY_AL'};
                        [LJOINT,FILE_LMVT]=buildTable(LJOINT,FILE_LMVT,FILE_LMVT.param.JointGraphSelectionBONETable,'BONE');
                        %                         for j=1:length(LJOINT)
                        if OK_GRAPH
                            SUPLABEL_ALLER=strcat('La t�che de',32,LabelName);
                            TITRE_ALLER= strcat('Les angles d''atteinte de la t�che . BONE model' );
                            SUPLABEL_RETOUR=strcat('La t�che de',32,LabelName);
                            TITRE_RETOUR= strcat('Les angles de retour de la t�che . BONE model' );
                            SUPLABEL_ALL=strcat('La t�che de',32,LabelName);
                            TITRE_ALL= strcat('le pattern des angles aller-retour de la t�che . BONE model' );
                            
                            [NPlot_ALLER,NPlot_RETOUR,NPlot_ALL,hfig2,hfig3,hfig4,FILE_LMVT]=GraphJointAnglesSHO (1,1,-1,NFIG,FILE_LMVT.param.JointGraphSelectionBONE,FILE_LMVT,LJOINT,SUPLABEL_ALLER,TITRE_ALLER,SUPLABEL_RETOUR,TITRE_RETOUR,SUPLABEL_ALL,TITRE_ALL);
                            fn_F1_ALLER_BONE=strcat(Pathout_FIG,FILE_LMVT.param.JointModel,'_GraphALLER_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_RETOUR_BONE=strcat(Pathout_FIG,FILE_LMVT.param.JointModel,'_GraphRETOUR_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_ALL_BONE=strcat(Pathout_FIG,FILE_LMVT.param.JointModel,'_GraphALL_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            export_fig(NPlot_ALLER,fn_F1_ALLER_BONE,'-png','-transparent','-a2','-opengl')
                            export_fig(NPlot_RETOUR,fn_F1_RETOUR_BONE,'-png','-transparent','-a2','-opengl')
                            export_fig(NPlot_ALL,fn_F1_ALL_BONE,'-png','-transparent','-a2','-opengl')
                        end
                        FILE_LMVT.param.JointGraphSelectionJOINT={'Global Thorax_AL';'Thorax_AL Clavicle2_AL';'Clavicle_AL Scapula_AL';'Scapula_AL Humerus3_AL';...
                            'Humerus3_AL ForearmY_AL';'ForearmY_AL HandY_AL'};
                        
                        %                         LJOINT(10).EULERNORM.ALLER.angle0(:,1,:)= -LJOINT(10).EULERNORM.ALLER.angle0(:,1,:);
                        %                         LJOINT(10).EULERNORM.ALLER.angle0(:,2,:)= -LJOINT(10).EULERNORM.ALLER.angle0(:,2,:);
                        %
                        %                         LJOINT(10).EULERNORM.RETOUR.angle0(:,1,:)= -LJOINT(10).EULERNORM.RETOUR.angle0(:,1,:);
                        %                         LJOINT(10).EULERNORM.RETOUR.angle0(:,2,:)= -LJOINT(10).EULERNORM.RETOUR.angle0(:,2,:);
                        %
                        %                         LJOINT(10).EULERZXY.angleM0(:,1,:)= -LJOINT(10).EULERZXY.angleM0(:,1,:);
                        %                         LJOINT(10).EULERZXY.angleM0(:,2,:)= -LJOINT(10).EULERZXY.angleM0(:,2,:);
                        
                        [LJOINT,FILE_LMVT]=buildTable(LJOINT,FILE_LMVT,FILE_LMVT.param.JointGraphSelectionJOINT,'JOIN');
                        
                        SUPLABEL_ALLER_JOINT=strcat('La t�che de',32,LabelName);
                        TITRE_ALLER_JOINT= strcat('Les angles d''atteinte de la t�che . JOINT model' );
                        SUPLABEL_RETOUR_JOINT=strcat('La t�che de',32,LabelName);
                        TITRE_RETOUR_JOINT= strcat('Les angles de retour de la t�che . JOINT model' );
                        SUPLABEL_ALL_JOINT=strcat('La t�che de',32,LabelName);
                        TITRE_ALL_JOINT= strcat('le pattern des angles aller-retour de la t�che . JOINT model' );
                        if OK_GRAPH
                            [NPlot_ALLER_JOINT,NPlot_RETOUR_JOINT,NPlot_ALL_JOINT,hfig5,hfig6,hfig7,FILE_LMVT]=GraphJointAnglesSHO (1,1,-1,NFIG+10,FILE_LMVT.param.JointGraphSelectionJOINT,FILE_LMVT,LJOINT,SUPLABEL_ALLER_JOINT,TITRE_ALLER_JOINT,SUPLABEL_RETOUR_JOINT,TITRE_RETOUR_JOINT,SUPLABEL_ALL_JOINT,TITRE_ALL_JOINT);
                            
                            fn_F1_ALLER_JOINT=strcat(Pathout_FIG,FILE_LMVT.param.JointModel,'_GraphALLER_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_RETOUR_JOINT=strcat(Pathout_FIG,FILE_LMVT.param.JointModel,'_GraphRETOUR_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_ALL_JOINT=strcat(Pathout_FIG,FILE_LMVT.param.JointModel,'_GraphALL_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            export_fig(NPlot_ALLER_JOINT,fn_F1_ALLER_JOINT,'-png','-transparent','-a2','-opengl')
                            export_fig(NPlot_RETOUR_JOINT,fn_F1_RETOUR_JOINT,'-png','-transparent','-a2','-opengl')
                            export_fig(NPlot_ALL_JOINT,fn_F1_ALL_JOINT,'-png','-transparent','-a2','-opengl')
                        end
                        FILE_LMVT.param_LR.LR_OUT_TABLE_STAT_ALLER=[FILE_LMVT.param.Table.BONE.OUT_RANGE_CLAIRE_ROM_Stat_cell2_ALLER ;FILE_LMVT.param.Table.JOIN.OUT_RANGE_CLAIRE_ROM_Stat_cell2_ALLER(2:end,:)];
                        FILE_LMVT.param_LR.LR_OUT_TABLE_STAT_RETOUR=[FILE_LMVT.param.Table.BONE.OUT_RANGE_CLAIRE_ROM_Stat_cell2_RETOUR ;FILE_LMVT.param.Table.JOIN.OUT_RANGE_CLAIRE_ROM_Stat_cell2_RETOUR(2:end,:)];
                        
                        
                        FilenameLJOINT_ALL_CTRL=strcat(Resu_ALL_dir_Norm,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','LJOINT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameLFILEstruct_ALL_CTRL=strcat(Resu_ALL_dir_Norm,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','FILE_LMVT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameLJOINT_ALL_PAT=strcat(Resu_ALL_dir_Patient,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','LJOINT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameLFILEstruct_ALL_PAT=strcat(Resu_ALL_dir_Patient,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','FILE_LMVT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        
                        FilenameLJOINT=strcat(Pathout_SAVE,FILE.SUBJECT.SubjectName,'_','LJOINT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameLFILEstruct=strcat(Pathout_SAVE,FILE.SUBJECT.SubjectName,'_','FILE_LMVT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        save(FilenameLJOINT,'LJOINT');
                        save(FilenameLFILEstruct,'FILE_LMVT');
                        Filename_FIG_ALLER_BONE=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALLER_BONE_ONEL','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_RETOUR_BONE=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_RETOUR_BONE_ONEL','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_ALL_BONE=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALL_BONE_ONEL','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_ALLER_JOINT=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALLER_JOINT_ONEL','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_RETOUR_JOINT=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_RETOUR_JOINT_ONEL','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.fig');
                        Filename_FIG_ALL_JOINT=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALL_JOINT_ONEL','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.fig');
                        if CTRL
                            [Pathout_Norm]=createdir2(Resu_ALL_dir_Norm,FILE.param.TASK);
                            [Pathout_Norm_Figure_ALLER] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALLER'));
                            [Pathout_Norm_Figure_RETOUR] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\RETOUR'));
                            [Pathout_Norm_Figure_ALL] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALL'));
                            fn_F1_ALLER_BONETOT=strcat(Pathout_Norm_Figure_ALLER,'GraphALLER_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_BONETOT=strcat(Pathout_Norm_Figure_RETOUR,'GraphRETOUR_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_BONETOT=strcat(Pathout_Norm_Figure_ALL,'GraphALL_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALLER_JOINTTOT=strcat(Pathout_Norm_Figure_ALLER,'GraphALLER_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_JOINTTOT=strcat(Pathout_Norm_Figure_RETOUR,'GraphRETOUR_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_JOINTTOT=strcat(Pathout_Norm_Figure_ALL,'GraphALL_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            
                            if OK_GRAPH
                                export_fig(hfig2,fn_F1_ALLER_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig3,fn_F1_RETOUR_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig4,fn_F1_ALL_BONETOT,'-png','-transparent','-a2','-opengl')
                                
                                export_fig(hfig5,fn_F1_ALLER_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig6,fn_F1_RETOUR_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig7,fn_F1_ALL_JOINTTOT,'-png','-transparent','-a2','-opengl')
                            end
                            save(FilenameLJOINT_ALL_CTRL,'LJOINT');
                            save(FilenameLFILEstruct_ALL_CTRL,'FILE_LMVT');
                        else
                            [Pathout_Patient]=createdir2(Resu_ALL_dir_Patient,FILE.param.TASK);
                            [Pathout_Patient_Figure] =createdir2(Resu_ALL_dir_Patient_Figure,FILE.param.TASK);
                            [Pathout_Patient_Figure_ALLER] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALLER'));
                            [Pathout_Patient_Figure_RETOUR] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\RETOUR'));
                            [Pathout_Patient_Figure_ALL] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALL'));
                            fn_F1_ALLER_BONETOT=strcat(Pathout_Patient_Figure_ALLER,'GraphALLER_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_BONETOT=strcat(Pathout_Patient_Figure_RETOUR,'GraphRETOUR_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_BONETOT=strcat(Pathout_Patient_Figure_ALL,'GraphALL_BONE_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALLER_JOINTTOT=strcat(Pathout_Patient_Figure_ALLER,'GraphALLER_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_JOINTTOT=strcat(Pathout_Patient_Figure_RETOUR,'GraphRETOUR_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_JOINTTOT=strcat(Pathout_Patient_Figure_ALL,'GraphALL_JOINT_ONEL_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            
                            if OK_GRAPH
                                export_fig(hfig2,fn_F1_ALLER_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig3,fn_F1_RETOUR_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig4,fn_F1_ALL_BONETOT,'-png','-transparent','-a2','-opengl')
                                
                                export_fig(hfig5,fn_F1_ALLER_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig6,fn_F1_RETOUR_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig7,fn_F1_ALL_JOINTTOT,'-png','-transparent','-a2','-opengl')
                            end
                            save(FilenameLJOINT_ALL_PAT,'LJOINT');
                            save(FilenameLFILEstruct_ALL_PAT,'FILE_LMVT');
                            
                        end
                        
                        if OK_GRAPH
                            %                             hgsave(hfig2,Filename_FIG_ALLER_BONE)
                            %                         hgsave(hfig3,Filename_FIG_RETOUR_BONE)
                            %                         hgsave(hfig4,Filename_FIG_ALL_BONE)
                            close(hfig2);close(hfig3);close(hfig4);
                            %                         hgsave(hfig5,Filename_FIG_ALLER_JOINT)
                            %                         hgsave(hfig6,Filename_FIG_RETOUR_JOINT)
                            %                         hgsave(hfig7,Filename_FIG_ALL_JOINT)
                            close(hfig5);close(hfig6);close(hfig7);
                        end %%if OK_GRAPH
                    end  %%%if (strcmp(FILE.param.BILATERALITY,'ONEL'))|| (strcmp(FILE.param.BILATERALITY,'BOTH'))
                    
                    if (strcmp(FILE.param.BILATERALITY,'ONER'))|| (strcmp(FILE.param.BILATERALITY,'BOTH'))
                        
                        FILE_RMVT.SUBJECT.SubjectName=FILE.SUBJECT.SubjectName;
                        FILE_RMVT.SESSION.datout_directory_ANALYSIS=FILE.SESSION.datout_directory_ANALYSIS;
                        FILE_RMVT.SESSION.Filename=FILE.SESSION.Filename;
                        FILE_RMVT.SESSION(Session).trialname=FILE.SESSION(Session).trialname;
                        FILE_RMVT.param.ClusterNameKinematicModel_AL=FILE.param.ClusterNameKinematicModel_AL;
                        FILE_RMVT.param.JointModel=FILE.param.JointModel;
                        FILE_RMVT.param.BILATERALITY=FILE.param.BILATERALITY;
                        FILE_RMVT.param.TASK=FILE.param.TASK;
                        FILE_RMVT.param.JOINT=FILE.param.JOINT;
                        FILE_RMVT.param.MODALITY=FILE.param.MODALITY;
                        FILE_RMVT.param.SIDE=1;
                        FILE_RMVT.param.CTRL_Chidren =FILE.param.CTRL_Chidren;
                        
                        FILE_RMVT=attibuteCutValues(acq,FILE_RMVT);
                        [FILE_RMVT]=attributeClusterUA(acq,FILE_RMVT,STATIC,'MVT');
                        [RSEGMENT_AL,FILE_RMVT]=extractSEGMENTstruct_REF(acq,FILE_RMVT,STATIC,'MVT',FILE.param.Gl2Pelvis);
                        SIDE_OF_TEST='R';
                        RJOINT(length(FILE_RMVT.param.JointModel ))=struct('name',[],'NFrame',[],'EULERZXY',[],'OVP',[],'GES',[]);%);
                        [RJOINT,FILE_RMVT]=extractJOINTstruct_MVT(acq,FILE_RMVT,RJOINT,RJOINTREF,RSEGMENT_AL,RSEGMENT_AL_REF,STATIC,SIDE_OF_TEST,'MVT');
                        [RJOINT,FILE_RMVT]=normalizeJOINTstrucSHOULDER(FILE_RMVT,RJOINT);
                        
                        
                        %%%{'Pelvis_AL Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Clavicle2_AL';'Thorax_AL Scapula_AL';'Thorax_AL Humerus_AL';'Thorax_AL Humerus3_AL';'Clavicle_AL Scapula_AL';'Scapula_AL Humerus_AL';'Scapula_AL Humerus3_AL';'Humerus3_AL ForearmY_AL';'Humerus3_AL ForearmZ_AL';'Humerus_AL ForearmY_AL';'Humerus_AL ForearmZ_AL';'ForearmZ_AL WristZ_AL';'ForearmY_AL WristY_AL';'WristY_AL WristZ_AL'}{'Humerus3_AL ForearmY_AL';'Humerus3_AL ForearmZ_AL';'Humerus_AL ForearmY_AL';'Humerus_AL ForearmZ_AL';'ForearmZ_AL WristZ_AL';'ForearmY_AL WristY_AL';'WristY_AL WristZ_AL'}
                        FILE_RMVT.param.JointGraphSelectionGLOBAL={'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Scapula_AL';'Thorax_AL Humerus3_AL';...
                            'Humerus3_AL ForearmY_AL';'ForearmY_AL HandY_AL'};
                        %                         FILE_RMVT.param.JointGraphSelectionGLOBAL={'Global Thorax_AL';'Thorax_AL Clavicle_AL';'Thorax_AL Scapula_AL';'Thorax_AL Humerus_AL';...
                        %                             'Humerus_AL ForearmY_AL';'ForearmY_AL HandY_AL'};
                        
                        RJOINT(11).EULERNORM.ALLER.angle0(:,3,:)=RJOINT(12).EULERNORM.ALLER.angle0(:,3,:);%%'Humerus3_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus3_AL ForearmZ_AL'
                        RJOINT(13).EULERNORM.ALLER.angle0(:,3,:)=RJOINT(14).EULERNORM.ALLER.angle0(:,3,:);%%'Humerus_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus_AL ForearmZ_AL'
                        RJOINT(15).EULERNORM.ALLER.angle0(:,2,:)=RJOINT(16).EULERNORM.ALLER.angle0(:,1,:);
                        
                        RJOINT(11).EULERNORM.RETOUR.angle0(:,3,:)=RJOINT(12).EULERNORM.RETOUR.angle0(:,3,:);
                        RJOINT(13).EULERNORM.RETOUR.angle0(:,3,:)=RJOINT(14).EULERNORM.RETOUR.angle0(:,3,:);
                        RJOINT(15).EULERNORM.RETOUR.angle0(:,2,:)=RJOINT(16).EULERNORM.RETOUR.angle0(:,1,:);
                        
                        RJOINT(11).EULERZXY.angleM0(:,3,:)=RJOINT(12).EULERZXY.angleM0(:,3,:);%%'Humerus3_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus3_AL ForearmZ_AL'
                        RJOINT(13).EULERZXY.angleM0(:,3,:)=RJOINT(14).EULERZXY.angleM0(:,3,:);%%'Humerus_AL ForearmY_AL' axe Z recoit la composante Z de'Humerus_AL ForearmZ_AL'
                        RJOINT(15).EULERZXY.angleM0(:,2,:)=RJOINT(16).EULERZXY.angleM0(:,1,:);
                        
                        [RJOINT,FILE_RMVT]=buildTable(RJOINT,FILE_RMVT,FILE_RMVT.param.JointGraphSelectionGLOBAL,'BONE');
                        
                        
                        if OK_GRAPH
                            SUPLABEL_ALLER=strcat('La t�che de',32,LabelName);
                            TITRE_ALLER= strcat('Les angles d''atteinte de la t�che . BONE model' );
                            SUPLABEL_RETOUR=strcat('La t�che de',32,LabelName);
                            TITRE_RETOUR= strcat('Les angles de retour de la t�che . BONE model' );
                            SUPLABEL_ALL=strcat('La t�che de',32,LabelName);
                            TITRE_ALL= strcat('le pattern des angles aller-retour de la t�che . BONE model' );
                            NFIG=90
                            [NPlot_ALLER,NPlot_RETOUR,NPlot_ALL,hfig2,hfig3,hfig4,FILE_RMVT]=GraphJointAnglesSHO(1,1,1,NFIG,FILE_RMVT.param.JointGraphSelectionGLOBAL,FILE_RMVT,RJOINT,SUPLABEL_ALLER,TITRE_ALLER,SUPLABEL_RETOUR,TITRE_RETOUR,SUPLABEL_ALL,TITRE_ALL);
                            %                         [SIDE_GRAPH,YLABEL_NEG,YLABEL_POS,YLABEL_SEQ,YLABEL_SEQ2,UNIT,TITLE,COLOR,LINEWIDTH,YMIN,YMAX,YINCR,XMIN,XMAX]=DefineGraphParameterShoulder(SIDE_OF_TEST,NFrameGraph,TYPMOTIONLeft);
                            %                         Graph_Name=('Upper Limb kinematics_Euler_Bone rotation');
                            %                         graph_Euler (Graph_Name,datout_directory_figure,Name_Patient,Subtitle,'BONE','EULER',YLABEL_NEG,YLABEL_POS,YLABEL_SEQ2,XLABEL,UNIT,TITLE,COLOR,YMIN,YMAX,YINCR,XMIN,XMAX,NFIG,SUBPLOT_N,SUBPLOT_M,POSITION,LINEWIDTH,SIDE_GRAPH,SIDE_OF_TEST,DATA_Left(XRight(1):XRight(end),:));
                            
                            cd (Pathout_FIG)
                            fn_F1_ALLER=strcat(Pathout_FIG,'GraphALLER_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_RETOUR=strcat(Pathout_FIG,'GraphRETOUR_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_ALL=strcat(Pathout_FIG,'GraphALL_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            export_fig(hfig2,fn_F1_ALLER,'-png','-transparent','-a2','-opengl')
                            export_fig(hfig3,fn_F1_RETOUR,'-png','-transparent','-a2','-opengl')
                            export_fig(hfig4,fn_F1_ALL,'-png','-transparent','-a2','-opengl')
                            
                        end
                        FILE_RMVT.param.JointGraphSelectionJOINT={'Global Thorax_AL';'Thorax_AL Clavicle2_AL';'Clavicle_AL Scapula_AL';'Scapula_AL Humerus3_AL';...
                            'Humerus3_AL ForearmY_AL';'ForearmY_AL HandY_AL'};
                        
                        [RJOINT,FILE_RMVT]=buildTable(RJOINT,FILE_RMVT,FILE_RMVT.param.JointGraphSelectionJOINT,'JOIN');
                        
                        if OK_GRAPH
                            SUPLABEL_ALLER_JOINT=strcat('La t�che de',32,LabelName);
                            TITRE_ALLER_JOINT= strcat('Les angles d''atteinte de la t�che . JOINT model' );
                            SUPLABEL_RETOUR_JOINT=strcat('La t�che de',32,LabelName);
                            TITRE_RETOUR_JOINT= strcat('Les angles de retour de la t�che . JOINT model' );
                            SUPLABEL_ALL_JOINT=strcat('La t�che de',32,LabelName);
                            TITRE_ALL_JOINT= strcat('le pattern des angles aller-retour de la t�che . JOINT model' );
                            
                            [NPlot_ALLER_JOINT,NPlot_RETOUR_JOINT,NPlot_ALL_JOINT,hfig5,hfig6,hfig7,FILE_RMVT]=GraphJointAnglesSHO (1,1,1,NFIG+10,FILE_RMVT.param.JointGraphSelectionJOINT,FILE_RMVT,RJOINT,SUPLABEL_ALLER_JOINT,TITRE_ALLER_JOINT,SUPLABEL_RETOUR_JOINT,TITRE_RETOUR_JOINT,SUPLABEL_ALL_JOINT,TITRE_ALL_JOINT);
                            
                            fn_F1_ALLER=strcat(Pathout_FIG,'GraphALLER_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_RETOUR=strcat(Pathout_FIG,'GraphRETOUR_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            fn_F1_ALL=strcat(Pathout_FIG,'GraphALL_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'.jpg');
                            export_fig(hfig5,fn_F1_ALLER,'-png','-transparent','-a2','-opengl')
                            export_fig(hfig6,fn_F1_RETOUR,'-png','-transparent','-a2','-opengl')
                            export_fig(hfig7,fn_F1_ALL,'-png','-transparent','-a2','-opengl')
                            
                        end
                        FILE_RMVT.param_LR.LR_OUT_TABLE_STAT_ALLER=[FILE_RMVT.param.Table.BONE.OUT_RANGE_CLAIRE_ROM_Stat_cell2_ALLER ;FILE_RMVT.param.Table.JOIN.OUT_RANGE_CLAIRE_ROM_Stat_cell2_ALLER(2:end,:)]
                        FILE_RMVT.param_LR.LR_OUT_TABLE_STAT_RETOUR=[FILE_RMVT.param.Table.BONE.OUT_RANGE_CLAIRE_ROM_Stat_cell2_RETOUR ;FILE_RMVT.param.Table.JOIN.OUT_RANGE_CLAIRE_ROM_Stat_cell2_RETOUR(2:end,:)]
                        
                        
                        FilenameRJOINT_ALL_CTRL=strcat(Resu_ALL_dir_Norm,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','RJOINT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameRFILEstruct_ALL_CTRL=strcat(Resu_ALL_dir_Norm,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','FILE_RMVT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameRJOINT_ALL_PAT=strcat(Resu_ALL_dir_Patient,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','RJOINT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameRFILEstruct_ALL_PAT=strcat(Resu_ALL_dir_Patient,'\',FILE.param.TASK,'\',FILE.SUBJECT.SubjectName,'_','FILE_RMVT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        
                        FilenameRJOINT=strcat(Pathout_SAVE,FILE.SUBJECT.SubjectName,'_','RJOINT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        FilenameRFILEstruct=strcat(Pathout_SAVE,FILE.SUBJECT.SubjectName,'_','FILE_RMVT','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_',FILE.param.BILATERALITY,'_','.mat');
                        save(FilenameRJOINT,'RJOINT');
                        save(FilenameRFILEstruct,'FILE_RMVT');
                        Filename_FIG_ALLER_BONE=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALLER_BONE_ONER','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_RETOUR_BONE=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_RETOUR_BONE_ONER','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_ALL_BONE=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALL_BONE_ONER','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_ALLER_JOINT=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALLER_JOINT_ONER','_',FILE.param.TASK,'_',FILE.param.MODALITY,'.fig');
                        Filename_FIG_RETOUR_JOINT=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_RETOUR_JOINT_ONER','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.fig');
                        Filename_FIG_ALL_JOINT=strcat(Pathout_FIG,FILE.SUBJECT.SubjectName,'_ALL_JOINT_ONER','_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.fig');
                        if CTRL
                            [Pathout_Norm]=createdir2(Resu_ALL_dir_Norm,FILE.param.TASK);
                            [Pathout_Norm_Figure_ALLER] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALLER'));
                            [Pathout_Norm_Figure_RETOUR] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\RETOUR'));
                            [Pathout_Norm_Figure_ALL] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALL'));
                            fn_F1_ALLER_BONETOT=strcat(Pathout_Norm_Figure_ALLER,'GraphALLER_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_BONETOT=strcat(Pathout_Norm_Figure_RETOUR,'GraphRETOUR_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_BONETOT=strcat(Pathout_Norm_Figure_ALL,'GraphALL_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALLER_JOINTTOT=strcat(Pathout_Norm_Figure_ALLER,'GraphALLER_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_JOINTTOT=strcat(Pathout_Norm_Figure_RETOUR,'GraphRETOUR_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_JOINTTOT=strcat(Pathout_Norm_Figure_ALL,'GraphALL_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            
                            if OK_GRAPH
                                export_fig(hfig2,fn_F1_ALLER_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig3,fn_F1_RETOUR_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig4,fn_F1_ALL_BONETOT,'-png','-transparent','-a2','-opengl')
                                
                                export_fig(hfig5,fn_F1_ALLER_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig6,fn_F1_RETOUR_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig7,fn_F1_ALL_JOINTTOT,'-png','-transparent','-a2','-opengl')
                            end
                            save(FilenameRJOINT_ALL_CTRL,'RJOINT');
                            save(FilenameRFILEstruct_ALL_CTRL,'FILE_RMVT');
                        else
                            [Pathout_Patient]=createdir2(Resu_ALL_dir_Patient,FILE.param.TASK);
                            [Pathout_Patient_Figure] =createdir2(Resu_ALL_dir_Patient_Figure,FILE.param.TASK);
                            [Pathout_Patient_Figure_ALLER] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALLER'));
                            [Pathout_Patient_Figure_RETOUR] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\RETOUR'));
                            [Pathout_Patient_Figure_ALL] =createdir2(Resu_ALL_dir_Norm_Figure,strcat(FILE.param.TASK,'\ALL'));
                            fn_F1_ALLER_BONETOT=strcat(Pathout_Patient_Figure_ALLER,'GraphALLER_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_BONETOT=strcat(Pathout_Patient_Figure_RETOUR,'GraphRETOUR_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_BONETOT=strcat(Pathout_Patient_Figure_ALL,'GraphALL_BONE_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALLER_JOINTTOT=strcat(Pathout_Patient_Figure_ALLER,'GraphALLER_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_RETOUR_JOINTTOT=strcat(Pathout_Patient_Figure_RETOUR,'GraphRETOUR_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            fn_F1_ALL_JOINTTOT=strcat(Pathout_Patient_Figure_ALL,'GraphALL_JOINT_ONER_',FILE.SUBJECT.SubjectName,'_',FILE.param.TASK,'_',FILE.param.MODALITY,'_','.jpg');
                            
                            if OK_GRAPH
                                export_fig(hfig2,fn_F1_ALLER_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig3,fn_F1_RETOUR_BONETOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig4,fn_F1_ALL_BONETOT,'-png','-transparent','-a2','-opengl')
                                
                                export_fig(hfig5,fn_F1_ALLER_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig6,fn_F1_RETOUR_JOINTTOT,'-png','-transparent','-a2','-opengl')
                                export_fig(hfig7,fn_F1_ALL_JOINTTOT,'-png','-transparent','-a2','-opengl')
                            end
                            save(FilenameRJOINT_ALL_PAT,'RJOINT');
                            save(FilenameRFILEstruct_ALL_PAT,'FILE_RMVT');
                            
                        end
                        %                         hgsave(hfig2,Filename_FIG_ALLER_BONE)
                        %                         hgsave(hfig3,Filename_FIG_RETOUR_BONE)
                        %                         hgsave(hfig4,Filename_FIG_ALL_BONE)
                        if OK_GRAPH
                            close(hfig2);close(hfig3);close(hfig4);
                            %                         hgsave(hfig5,Filename_FIG_ALLER_JOINT)
                            %                         hgsave(hfig6,Filename_FIG_RETOUR_JOINT)
                            %                         hgsave(hfig7,Filename_FIG_ALL_JOINT)
                            close(hfig5);close(hfig6);close(hfig7);
                        end %%%if OK_GRAPH
                    end %%if (strcmp(FILE.param.BILATERALITY,'ONER'))|| (strcmp(FILE.param.BILATERALITY,'BOTH'))
                    %NFIG=NFIG+10-1;
                    
                    %                     [OUT_PROFILEScore_header,OUT_PROFILEScore_header2,OUT_PROFILEScore_header3]=ProfilScoreLR(LJOINT,RJOINT,TYPMOTION2,Norm_DATA_reduced_ALL_AVG,Norm_DATA_Current_reduced_ALL,Norm_DATA_JOINT_reduced_ALL_AVG,Norm_DATAJOINT_Current_reduced_ALL)
                    %
                    %
                    %                     h(1)=openfig(Filename_FIG_ALL_JOINT);
                    %                     h(2)=openfig(Filename_FIG_ALL_BONE);
                    %                     %a= randn(1,100) ; b = randn(1,150) ;
                    %                    % h(1) = figure; plot(a); h(2) = figure; plot(b);
                    %                     handleLine = findobj(h,'type','line');
                    %                     hold on ;
                    %                     for i = 1 : length(handleLine)
                    %                         plot(get(handleLine(i),'XData'), get(handleLine(i),'YData')) ;
                    %                     end
                    
                    FILE.Filename_ALL_SUBJECTS(j,:)=  {FileName};
                    clear FILE_RMVT RJOINT RSEGMENT_AL FILE_LMVT LJOINT  LSEGMENT_AL
                    %                       [OUT_DBEuler_Header,OUT_DBEuler_DATA,FILE_RMVT]=writeJOINTOFFSET(FILE_RMVT,RJOINT,FILE.param.TASK,FILE.param.MODALITY)
                end  %%for j=1:Dim_MOTION(2)
            end  %%if OK_MOTION_PROCESS
        end %if MOTIONFOOTKINEMATICS
    end  %%% for Session=1:DimRoot_Session(2)
    
    if COMPARENORM
        FILE_COMP=FILE;
    end
    clear FILE   RJOINTREF LJOINTREF LSEGMENT_AL_REF RSEGMENT_AL_REF
    
end  %%for Sujet=1:size(Folder0,1)



if OK_POPNORM
    TASK=[{'MT'};{'ABD'};{'COOKIES'};{'BRI'}; {'RIRE'};{'R2'};{'TRANSUP'}];
    %     TASK={'R2'}
    %TASK={'TRANSUP'}
    BEST_OK=1;
    OK_EXCEL_WRITE=0;
    FAST_OK=0;
    tvalue=1;
    
    for n=1:length(TASK)
        %switch char(TASK(n))
        %case {'MT','ABD','COOKIES'}
        %case {'ABD'}
        
        popnormUE(OK_EXCEL_WRITE,BEST_OK,FAST_OK,char(TASK(n)),Resu_ALL_dir_Norm,Resu_ALL_dir_Norm_XLS,Resu_ALL_dir_Norm_Figure,RightNorm,tvalue);
        %end
    end
end %%%for ii=1:size(Folder0,1)

%end
%COMPARENORM=0;
if COMPARENORM
    TASK=[{'MT'} {'ABD'} {'COOKIES'} {'BRI'} {'RIRE'} {'TRANSUP'}];%%%{'R2'}
    
    for n=1:length(TASK)
        %switch char(TASK(n))
        %case {'MT'}
        Task=char(TASK(n));
        %Task='RIRE';
        %TYPofTASK='Main t�te';
        MODALITY='BEST';
        TYP_SESSION='Pre';
        grey=[0.8  0.8  0.8];
        lightBlue=[0.3,0.3,1];
        lightgrey=[0.9  0.9  1];
        DirSave=strcat(Resu_ALL_dir_Patient,Task,'\');
        
        %                     L_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,R_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,...
        %             L_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN,R_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN,...
        %
        
        [S_NORM_ALLER,S_NORM_RETOUR,S_NORM_ALLER2,S_NORM_RETOUR2,S_PATIENT_FILE_ALLER_RIGHT,S_PATIENT_FILE_ALLER_LEFT,...
            L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,...
            L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN,R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN,...
            L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX,R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX,...
            L_PATIENT_OUT_TABLE_STAT_RETOUR_ALLSUBJECT_BEST_BONEJOIN_MINMAX,R_PATIENT_OUT_TABLE_STAT_RETOUR_ALLSUBJECT_BEST_BONEJOIN_MINMAX,...
            R_GVS_ALLER,R_GVS_RETOUR,L_GVS_ALLER,L_GVS_RETOUR,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_AVG,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_AVG,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER2,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER2,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER4,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER4,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_UPPER2,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_LOWER2,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_UPPER4,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_LOWER4,...
            GraphYLabelPos,GraphYLabelNeg,GraphUnit,GraphYINCR,GraphYMIN,GraphYMAX,GraphTitle,TYPofTASK]=LoadStructTask(Folder0,Task,Resu_ALL_dir_Patient,RightNorm);
        
        Name_Patient=strcat('P_',PatientFirstName ,'_',PatientLastName);
        Graph_Name_ALLER=strcat('Les angles d''atteinte de la t�che',32,32,TYPofTASK);
        
        %                  graphPREPOST2(NFIG,SUBPLOT_N,SUBPLOT_M,TASK,MODALITY,TYP_SESSION,SIDE_GRAPH,...
        %                     ColorCorridor,ColorCorridor2,COLORLeftSide,COLORRightSide,LINEWIDTH,LINESTYLE,XMIN,XMAX,Name_Patient,Graph_Name,GraphYLabelPos,GraphYLabelNeg,GraphUnit,GraphYINCR,GraphYMIN,GraphYMAX,GraphTitle,...
        %                     LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER2,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER2,R_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,L_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN);
        OK_BIGTITLE=1;
        [hfig]=graphPREPOST2(15,OK_BIGTITLE,7,3,Task,MODALITY,TYP_SESSION,'Both',...
            grey,lightgrey,'r','b',2,'-',1,101,Name_Patient,Graph_Name_ALLER,GraphYLabelPos,GraphYLabelNeg,GraphUnit,GraphYINCR,GraphYMIN,GraphYMAX,GraphTitle,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER2,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER2,R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN);
        %D:\DataMatlab\Gonio10\Gait_inVIVO\Data\Shoulder_Model_Claire2\P47_BURGEONHUGO\Session1_PRE\DATAOUT\report\FIGURE
        
        
        fn_PAGE_ALLER_pdf=strcat(FILE_COMP.SESSION.datout_directory_REPORT_FIGURE,'ALLER','\','OUTPDF','\','PAGE',32,Task,'.pdf');
        export_fig(hfig,fn_PAGE_ALLER_pdf,'-pdf','-transparent','-a2','-painters')
        
        close(hfig)
        Graph_Name_RETOUR=strcat('Les angles de retour de la t�che',32,32,TYPofTASK);
        
        %                  graphPREPOST2(NFIG,SUBPLOT_N,SUBPLOT_M,TASK,MODALITY,TYP_SESSION,SIDE_GRAPH,...
        %                     ColorCorridor,ColorCorridor2,COLORLeftSide,COLORRightSide,LINEWIDTH,LINESTYLE,XMIN,XMAX,Name_Patient,Graph_Name,GraphYLabelPos,GraphYLabelNeg,GraphUnit,GraphYINCR,GraphYMIN,GraphYMAX,GraphTitle,...
        %                     LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER2,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER2,R_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN,L_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN);
        OK_BIGTITLE=1;
        [hfigR]=graphPREPOST2(16,OK_BIGTITLE,7,3,Task,MODALITY,TYP_SESSION,'Both',...
            grey,lightgrey,'r','b',2,'-',1,101,Name_Patient,Graph_Name_RETOUR,GraphYLabelPos,GraphYLabelNeg,GraphUnit,GraphYINCR,GraphYMIN,GraphYMAX,GraphTitle,...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_UPPER4,LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_LOWER4,R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN,L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN);
        %D:\DataMatlab\Gonio10\Gait_inVIVO\Data\Shoulder_Model_Claire2\P47_BURGEONHUGO\Session1_PRE\DATAOUT\report\FIGURE
        
        
        fn_PAGE_RETOUR_pdf=strcat(FILE_COMP.SESSION.datout_directory_REPORT_FIGURE,'RETOUR','\','OUTPDF','\','PAGE',32,Task,'.pdf');
        export_fig(hfigR,fn_PAGE_RETOUR_pdf,'-pdf','-transparent','-a2','-painters')
        close(hfigR)
        %%Benedetti
        
        L_PTA_PATIENT_AVG(:,n)= L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1);
        R_PTA_PATIENT_AVG(:,n)= R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1);
        L_ROM_PATIENT_AVG(:,n)= L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4);
        R_ROM_PATIENT_AVG(:,n)= R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4);
        L_AVS_PAT_MED(:,n)=L_GVS_ALLER(:,1);
        R_AVS_PAT_MED(:,n)= R_GVS_ALLER(:,1);
        L_AVS_PAT_AVG(:,n)= R_GVS_ALLER(:,2);
        R_AVS_PAT_AVG(:,n)= R_GVS_ALLER(:,2);
        
        PTA_NORM_AVG(:,n)=S_NORM_ALLER2.LR_NORM_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1);
        ROM_NORM_AVG(:,n)=S_NORM_ALLER2.LR_NORM_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4);
        AVS_NORM_MED(:,n)= S_NORM_ALLER2.GVS21_MEDIAN_ALLER_BEST_BONEJOIN';
        AVS_NORM_AVG(:,n)= S_NORM_ALLER2.GVS21_MEAN_ALLER_BEST_BONEJOIN';
        AVS_NORM_STD(:,n)= S_NORM_ALLER2.GVS21_STD_ALLER_BEST_BONEJOIN';
        PTA_NORM_STD(:,n)=S_NORM_ALLER2.LR_NORM_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,5);
        ROM_NORM_STD(:,n)=S_NORM_ALLER2.LR_NORM_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,8);
        L_PTA_ZSCORE(:,n)=(PTA_NORM_AVG(:,n) -L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1))./PTA_NORM_STD(:,n);
        R_PTA_ZSCORE(:,n)=(PTA_NORM_AVG(:,n) -R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1))./PTA_NORM_STD(:,n);
        L_ROM_ZSCORE(:,n)=(ROM_NORM_AVG(:,n) -L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4))./ROM_NORM_STD(:,n);
        R_ROM_ZSCORE(:,n)=(ROM_NORM_AVG(:,n) -R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4))./ROM_NORM_STD(:,n);
        %S_NORM_ALLER.LR_OUT_TABLE_STAT_ALLER_ALLSUBJECT_ALLER_BEST_BONEJOIN
        
        %%Profilscore
        OUT_TABLE_ALLER(:,:,n)=[L_GVS_ALLER(:,1)';...
            L_GVS_ALLER(:,2)';...
            R_GVS_ALLER(:,1)';...
            R_GVS_ALLER(:,2)';...
            S_NORM_ALLER2.GVS21_MEDIAN_ALLER_BEST_BONEJOIN;...
            S_NORM_ALLER2.GVS21_MEAN_ALLER_BEST_BONEJOIN;...
            L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1)';...
            R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,1)';...
            PTA_NORM_AVG(:,n)';...
            PTA_NORM_STD(:,n)';...
            L_PTA_ZSCORE(:,n)';...
            R_PTA_ZSCORE(:,n)';...
            L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4)';...
            R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX(:,4)';...
            ROM_NORM_AVG(:,n)';...
            ROM_NORM_STD(:,n)';...
            L_ROM_ZSCORE(:,n)';...
            R_ROM_ZSCORE(:,n)'];
        
        
        
        
        clear    S_NORM_ALLER S_NORM_RETOUR S_PATIENT_FILE_ALLER_RIGHT S_PATIENT_FILE_ALLER_LEFT ...
            L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN ...
            L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN ...
            L_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX R_PATIENT_OUT_TABLE_STAT_ALLER_ALLSUBJECT_BEST_BONEJOIN_MINMAX ...
            L_PATIENT_OUT_TABLE_STAT_RETOUR_ALLSUBJECT_BEST_BONEJOIN_MINMAX R_PATIENT_OUT_TABLE_STAT_RETOUR_ALLSUBJECT_BEST_BONEJOIN_MINMAX...
            R_GVS_ALLER R_GVS_RETOUR L_GVS_ALLER L_GVS_RETOUR ...
            L_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN R_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN ...
            L_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN R_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN ...
            L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN ...
            L_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN R_PATIENT_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN ...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_AVG LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_AVG ...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER2 LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER2 ...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_UPPER4 LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_ALLER_BEST_BONEJOIN_LOWER4 ...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_UPPER2 LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_LOWER2 ...
            LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_UPPER4 LR_NORM_OUT_TABLE_STAT_ALLSUBJECT_RETOUR_BEST_BONEJOIN_LOWER4 ...
            GraphYLabelPos GraphYLabelNeg GraphUnit GraphYINCR GraphYMIN GraphYMAX GraphTitle
        % end %%switch char(TASK(1))
        
    end %%for n=1:length(TASK)
end


%%%Global arm variable score
TASK=[{'MT'} {'ABD'} {'COOKIES'} {'BRI'} {'RIRE'}  {'TRANSUP'}];%%{'R2'}
for nn=1: size(L_AVS_PAT_MED,1)
    for n=1:length(TASK)
        
        OUT_L_PATIENT_APS_ALL(nn,:)=nanmedian(L_AVS_PAT_MED(nn,n));
        OUT_R_PATIENT_APS_ALL(nn,:)=nanmedian(R_AVS_PAT_MED(nn,n));
        OUT_LR_NORM_APS_ALL(nn,:)=nanmedian(AVS_NORM_MED(nn,n));
        %OUT_L_PATIENT_APS_DDL1(nn,:)=nanmedian(L_AVS_PAT_MED(nn,n));
    end
end
OUT_L_PATIENT_APS_ALL_GL=nanmedian(OUT_L_PATIENT_APS_ALL);
OUT_R_PATIENT_APS_ALL_GL=nanmedian(OUT_R_PATIENT_APS_ALL);
OUT_LR_NORM_APS_ALL_GL=nanmedian(OUT_LR_NORM_APS_ALL);
%OUT_L_PATIENT_APS_DDL1_GL=nanmedian(OUT_L_PATIENT_APS_DDL1);
% OUT_L_PATIENT_APS_ALL_GL=nanmedian(OUT_L_PATIENT_APS_ALL([));
% OUT_R_PATIENT_APS_ALL_GL=nanmedian(OUT_R_PATIENT_APS_ALL);
% OUT_LR_NORM_APS_ALL_GL=nanmedian(OUT_LR_NORM_APS_ALL);

for n=1:length(TASK)
    switch char(TASK(n))
        case 'MT'
            OUT_L_PATIENT_APS_ALL(n,:)=nanmedian(L_AVS_PAT_MED(:,n));
            OUT_R_PATIENT_APS_ALL(n,:)=nanmedian(R_AVS_PAT_MED(:,n));
            OUT_LR_NORM_APS_ALL(n,:)=nanmedian(AVS_NORM_MED(:,n));
            OUT_L_PATIENT_APS_DDL1(n,:)=nanmedian(L_AVS_PAT_MED(:,n));
            OUT_R_PATIENT_APS_DDL1(n,:)=nanmedian(R_AVS_PAT_MED(:,n));
            OUT_LR_NORM_APS_DDL1(n,:)=nanmedian(AVS_NORM_MED(:,n));
            OUT_L_PATIENT_APS_DDL2(n,:)=nanmedian(L_AVS_PAT_MED(:,n));
            OUT_R_PATIENT_APS_DDL2(n,:)=nanmedian(R_AVS_PAT_MED(:,n));
            OUT_LR_NORM_APS_DDL2(n,:)=nanmedian(AVS_NORM_MED(:,n));
            
        case 'ABD'
            
        case 'COOKIES'
            OUT_L_PATIENT_APS_ALL(n,:)=nanmedian(L_AVS_PAT_MED(:,n));
            OUT_R_PATIENT_APS_ALL(n,:)=nanmedian(R_AVS_PAT_MED(:,n));
            OUT_LR_NORM_APS_ALL(n,:)=nanmedian(AVS_NORM_MED(:,n));
            OUT_L_PATIENT_APS_DDL1(n,:)=nanmedian(L_AVS_PAT_MED(:,n));
            OUT_R_PATIENT_APS_DDL1(n,:)=nanmedian(R_AVS_PAT_MED(:,n));
            OUT_LR_NORM_APS_DDL1(n,:)=nanmedian(AVS_NORM_MED(:,n));
            OUT_L_PATIENT_APS_DDL2(n,:)=nanmedian(L_AVS_PAT_MED(:,n));
            OUT_R_PATIENT_APS_DDL2(n,:)=nanmedian(R_AVS_PAT_MED(:,n));
            OUT_LR_NORM_APS_DDL2(n,:)=nanmedian(AVS_NORM_MED(:,n));
            
            
        case 'BRI'
        case 'RIRE'
        case 'R2'
        case 'TRANSUP'
    end
    
    
end
OK_WRITE_REPORT=1;
if OK_WRITE_REPORT
    
    
    Myfile1=strcat(default_directory,separator,'Model_Report_UE.docx');
    MyFolder=strcat(TRANSFERT_REPORT_ALL_dir,'Model_Report_UE.docx');
    
    
    status=copyfile( Myfile1,MyFolder )
    [fn_PAGE0_pdf,FILESTRINGOUT]=writeWORD(FILE_COMP);
    
    filenameOUT_DOC=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_DOC,FILESTRINGOUT);
    filenameOUT_DOC1=strcat(FILE_COMP.TRANSFERT_REPORT_ALL_dir,FILESTRINGOUT);
    fn_PAGE2_pdf=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_ALLER_OUTPDF,'PAGE COOKIES','.pdf');
    fn_PAGE3_pdf=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_ALLER_OUTPDF,'PAGE MT','.pdf');
    fn_PAGE4_pdf=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_ALLER_OUTPDF,'PAGE BRI','.pdf');
    fn_PAGE5_pdf=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_ALLER_OUTPDF,'PAGE RIRE','.pdf');
    fn_PAGE6_pdf=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_ALLER_OUTPDF,'PAGE R2','.pdf');
    fn_PAGE7_pdf=strcat(FILE_COMP.SESSION(FILE_COMP.Session).datout_directory_REPORT_ALLER_OUTPDF,'PAGE TRANSUP','.pdf');
    
    % append_pdfs(filenameOUT, fn_PAGE0_pdf,fn_PAGE2_pdf,fn_PAGE3_pdf,fn_PAGE4_pdf,fn_PAGE5_pdf,fn_PAGE6_pdf,fn_PAGE7_pdf)
    append_pdfs(filenameOUT_DOC,fn_PAGE0_pdf,fn_PAGE2_pdf,fn_PAGE3_pdf,fn_PAGE4_pdf,fn_PAGE5_pdf,fn_PAGE6_pdf,fn_PAGE7_pdf)
    append_pdfs(filenameOUT_DOC1,fn_PAGE0_pdf,fn_PAGE2_pdf,fn_PAGE3_pdf,fn_PAGE4_pdf,fn_PAGE5_pdf,fn_PAGE6_pdf,fn_PAGE7_pdf)
    
end  %%if OK_WRITE_REPORT
%                 if TypRegion=='L'
%                     if  OK_LFAST==0;
%                         TITRE1='Mouvements r�alis�s le plus vite possible. Donn�es normalis�es (ROM LBEST ASSIS)';
%                         paperorientation='portrait';
%                         FS=0;
%                         SIZEPAPER='A5';
%                         hfig3= initgraph2(FILE,1,' ',9,'Times New Roman',0,paperorientation,FS,SIZEPAPER);
%                         f=figure(hfig3)
%                         subplot('Position',[0.3 0.5 0.01 0.2]);
%                         text(0.5,0.5,'T�che FAST  non r�alis�e.Donn�es normalis�es (ROM LBEST ASSIS)'); axis off
%                         %                             p=mtit(TITRE1,'fontsize',20,'color',[1 0 0], 'xoff',0,'yoff',0);
%                         %                             set(p.th,'edgecolor',.5*[1 1 1]);
%
%                         fn_PAGE3_pdf=strcat(FILE.SESSION.datout_directory_report_figure,'FAST','\','PAGE3','.pdf');
%                         export_fig(hfig3,fn_PAGE3_pdf,'-pdf','-transparent','-a2','-painters')
%
%                         TITRE2='Mouvements r�alis�s le plus vite possible. Portraits de phase (LFAST)';
%                         hfig4= initgraph2(FILE,1,' ',9,'Times New Roman',0,paperorientation,FS,SIZEPAPER);
%                         f=figure(hfig4)
%                         subplot('Position',[0.3 0.5 0.01 0.2]);
%                         text(0.5,0.5,'T�che FAST  non r�alis�e.Portraits de phase'); axis off
%                         %                             p=mtit(TITRE1,'fontsize',20,'color',[1 0 0], 'xoff',0,'yoff',0);
%                         %                             set(p.th,'edgecolor',.5*[1 1 1]);
%
%                         fn_PAGE4_pdf=strcat(FILE.SESSION.datout_directory_report_figure,'FAST','\','PAGE4','.pdf');
%                         export_fig(hfig4,fn_PAGE4_pdf,'-pdf','-transparent','-a2','-painters')
%
%                         TITRE3='Rythme lombo-pelvien Mouvement r�alis� le plus vite possible. (LFAST) ';
%                         hfig6= initgraph2(FILE,1,' ',9,'Times New Roman',0,paperorientation,FS,SIZEPAPER);
%                         f=figure(hfig6)
%                         subplot('Position',[0.3 0.5 0.01 0.2]);
%                         text(0.5,0.5,'T�che FAST  non r�alis�e.Rythme lombo-pelvien'); axis off
%                         %                             p=mtit(TITRE3,'fontsize',20,'color',[0 0 0], 'xoff',0,'yoff',0);
%                         %                             set(p.th,'edgecolor',.5*[1 1 1]);
%
%                         fn_PAGE6_pdf=strcat(FILE.SESSION.datout_directory_report_figure,'FAST','\','PAGE6','.pdf');
%                         export_fig(hfig6,fn_PAGE6_pdf,'-pdf','-transparent','-a2','-painters')
%                         OUT_RANGE_EULERNORM3_FAST_color=OUT_RANGE_EULERNORM3_BEST_color;
%                         OUT_RANGE_EULERNORM3_FAST_color(3:end,2:end)={' '};
%                     end



%end  %%if COMPARENORM
